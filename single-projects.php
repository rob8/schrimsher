<?php get_header(); ?>

    <?php echo schrimsher_build_header('property', $post->ID); ?>
    
    <div class="page-content">
        
        <div class="property-intro">
        
            <div class="container-fluid">
                
                <div class="row">
                
                    <div class="col-xs-12 col-md-8" data-mh="property-top-group">
                        
                        <h2>Project Overview</h2>
                        
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        
                        	<?php the_content(); ?>
                        
                        <?php endwhile; else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                        <?php endif; ?>
                        
                    </div>
                    
                
                </div>
                
                <?php 
                    $gallery = get_post_meta($post->ID, '_property_attribute_gallery', true);
                    
                    if (!empty($gallery)) {
                ?>
                
                    <div class="row">
                        
                        <div class="col-xs-12 property-gallery">
                            
                            <?php echo do_shortcode('[envira-gallery id="'.$gallery.'"]'); ?>
                            
                        </div>
                        
                    </div>
                
                <?php
                    }
                ?>
                
            </div>
        
        </div>

        <?php 
            $properties_link = get_post_meta($post->ID, '_property_attribute_link', true);

            if (!empty($properties_link)) {
        ?>
        
            <div class="property-availabilities">
            
                <div class="container-fluid">
                    
                    <div class="row">
                        
                        <div class="col-xs-12">
                            
                            
                            <?php

                                $headline_override = get_theme_mod('project_availabilities_headline');
                                echo '<h2>' . ( ! empty( $headline_override ) ? esc_html( $headline_override ) : 'Availabilities' ) . '</h2>';

                                $text_override = get_theme_mod('project_availabilities_text');
                                if ( ! empty( $text_override ) ) {
                                    $text_override = str_replace('[link]', '<a href="' .  $properties_link . '">', $text_override);
                                    $text_override = str_replace('[/link]', '</a>', $text_override);
                                    echo '<p class="lead">' . $text_override . '</p>';
                                } else {
                                    echo '<p class="lead">To view availabilities for this project, visit <a href="' . $properties_link . '">it\'s page</a> on Schrimsher Properties.</p>';
                                }
                            ?>
                            
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>

        <?php
            }
        ?>
        
        <?php
            $pdfs = get_post_meta($post->ID, '_property_floorplans_items', true);
            if (!empty($pdfs)) {
        ?>
        
            <div class="property-floorplans">
            
                <div class="container-fluid">
                    
                    <div class="row">
                        
                        <div class="col-xs-12">
                            
                            <h2>Floor Plans</h2>
                            
                            <div class="property-floorplans-items">
                                
                                <div class="row">
                                    
                                    <?php
                                    
                                        
                                        
                                        $output = '';
                                        
                                        if (!empty($pdfs)) {
                                            
                                            foreach ($pdfs as $pdf) {
                                                
                                                $output .= '
                                                    <div class="col-xs-12 col-sm-4 floorplan-item">
                                                        <div class="floorplan-item-inner">
                                                            '.(!empty($pdf['pdf']) ? '<a href="'.$pdf['pdf'].'">' : '').wp_get_attachment_image($pdf['image_id'],'highlighted',"", array( "class" => "img-responsive" )).(!empty($pdf['pdf']) ? '</a>' : '').'
                                                            '.(!empty($pdf['pdf']) ? '<a target="_blank" href="'.$pdf['pdf'].'" class="btn btn-lg btn-primary btn-block">'.(!empty($pdf['title']) ? $pdf['title'] : '').'</a>' : '').'
                                                        </div>
                                                    </div>
                                                ';
                                                
                                            }
                                            
                                        }
                                        
                                        echo $output;
                                        
                                    ?>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
        
        <?php } ?>
        
    </div>
    
    <?php 
        $hero_img = get_post_meta($post->ID, '_property_hero_image_id', true);

        if (!empty($hero_img)) {

        $hero_src = wp_get_attachment_image_src($hero_img, 'large');
        $hero_src = $hero_src[0];
    ?>

        <div class="property-hero" style="background-image: url(<?php echo $hero_src; ?>);">

            &nbsp;

        </div>

    <?php
        }
    ?>
    
<?php get_footer(); ?>