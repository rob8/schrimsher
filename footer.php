        <footer>
            <?php 
            
                $footer_display = get_theme_mod('robust_footer');
                
                if ($footer_display == 'on') {
                
            ?>
            <div class="footer-top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 footer-top-menu">
                            <h4>Quick Links</h4>
                            <?php
                            	wp_nav_menu( array('theme_location' => 'footer_col1', 'depth' => 1, 'fallback_cb' => false )); 
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 footer-top-menu">
                            <h4>Featured Properties</h4>
                            <?php
                            	wp_nav_menu( array('theme_location' => 'footer_col2', 'depth' => 1, 'fallback_cb' => false )); 
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 footer-top-address">
                            <?php
                                $contact_info['addr1'] = get_theme_mod('contact_addr1');
                                $contact_info['addr2'] = get_theme_mod('contact_addr2');
                                $contact_info['addr3'] = get_theme_mod('contact_addr3');
                                $contact_info['phone'] = get_theme_mod('contact_phone');
                                $contact_info['email'] = get_theme_mod('contact_email');
                                $contact_info['fax'] = get_theme_mod('contact_fax');
                                $contact_info['fax_display'] = get_theme_mod('contact_fax_display');
                                
                                $output = '<h4>Get In Touch</h4>';
                                
                                $output .= '<p>
                                    '.( !empty($contact_info['addr1']) ? '<span>'.$contact_info['addr1'].'</span>' : '' ).'
                                    '.( !empty($contact_info['addr2']) ? '<span>'.$contact_info['addr2'].'</span>' : '' ).'
                                    '.( !empty($contact_info['addr3']) ? '<span>'.$contact_info['addr3'].'</span>' : '' ).'
                                    '.( !empty($contact_info['phone']) ? '<span>p: '.$contact_info['phone'].'</span>' : '' ).'
                                    '.( !empty($contact_info['email']) ? '<span>e: <a href="mailto:'.$contact_info['email'].'">'.$contact_info['email'].'</a></span>' : '' ).'
                                    '.( $contact_info['fax_display'] == 'on' ? ( !empty($contact_info['fax']) ? '<span>f: '.$contact_info['fax'].'</span>' : '' ) : '' ).'
                                </p>';
                                
                                echo $output;
                            ?>
                            
                        </div>
                        <!--
                        <div class="col-xs-12 col-sm-3 footer-top-newsletter">
                            <h4>Newsletter</h4>
                            <p>Sign up to receive updates on availabilities and new developments!</p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newsletterModal">
                              Sign Up!
                            </button>
                        </div>
                        -->
                    </div>
                </div>            
            </div>
            
            <?php 
            
                }
                
            ?>
            <div class="footer-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 footer-bottom-copyright">
                            <?php 
                                
                                //Footer Logo
                                
                                $logo = get_theme_mod('site_logo');
                                $site_ia = get_theme_mod( 'site_ia' );
                                
                                if (!empty($logo)) {
                                    $logo = Kirki_Helper::get_image_id( $logo );
                                    $logo = '<span class="footer-logo">'.wp_get_attachment_image( $logo, array('700', '41'), "" ).'</span>';
                                }
                                
                                echo $logo;
                                
                                //Footer Copyright
                                
                                $copyright = get_theme_mod('site_copyright');
                                
                                echo '<span class="footer-copyright">';
                                if (!empty($copyright)) {
                                    echo $copyright.( $site_ia != 'company' ? ', a <a href="https://schrimshercompany.com/" target="_blank">Schrimsher Company</a>' : '' ).'<br>';
                                }
                                echo 'Built by <a href="https://pixelwatt.com">PixelWatt</a></span>';
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 footer-bottom-social">
                            <ul>
                                <?php echo schrimsher_social_links(array('facebook','twitter','linkedin','instagram')); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
        
        <!-- Modal -->
        <!-- 
        <div class="modal fade" id="newsletterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Newsletter</h4>
              </div>
              <div class="modal-body">
                <?php
                    $newsletter_form = get_theme_mod('newsletter_form_id');
                    
                    echo ( !empty($newsletter_form) ? do_shortcode('[ninja_form id='.$newsletter_form.']') : '');    
                ?>
              </div>
            </div>
          </div>
        </div>
        -->
        
        <!-- Modal -->
        <div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="emailModalLabel">Send to Email</h4>
              </div>
              <div class="modal-body">
                <?php
                    $email_form = get_theme_mod('email_form_id');
                    
                    echo ( !empty($email_form) ? do_shortcode('[ninja_form id='.$email_form.']') : '');    
                ?>
              </div>
            </div>
          </div>
        </div>
        
        <?php wp_footer(); ?>
        
        <?php
	        if ($site_ia != 'company') {
		        echo '
		        <!-- Global site tag (gtag.js) - Google Analytics -->
				<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108229144-2"></script>
				<script>
				  window.dataLayer = window.dataLayer || [];
				  function gtag(){dataLayer.push(arguments);}
				  gtag(\'js\', new Date());
				
				  gtag(\'config\', \'UA-108229144-2\');
				</script>
		        ';
	        } else {
		        echo '
		        <!-- Global site tag (gtag.js) - Google Analytics -->
				<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108229144-1"></script>
				<script>
				  window.dataLayer = window.dataLayer || [];
				  function gtag(){dataLayer.push(arguments);}
				  gtag(\'js\', new Date());
				
				  gtag(\'config\', \'UA-108229144-1\');
				</script>
		        ';
	        }
        ?>
        
        
    </body>
</html>