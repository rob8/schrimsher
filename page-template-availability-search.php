<?php 
/* 
Template Name: Availability Search
*/ 
?>

<?php get_header(); ?>

    <script type="text/javascript">
    
        jQuery(function($){
        	$('#filter').submit(function(){
        		var filter = $('#filter');
        		$.ajax({
        			url:filter.attr('action'),
        			data:filter.serialize(), // form data
        			type:filter.attr('method'), // POST
        			beforeSend:function(xhr){
        				filter.find('button').text('Processing...'); // changing the button label
        			},
        			success:function(data){
        				filter.find('button').text('Update Results'); // changing the button label back
        				$('#response').html(data); // insert data
        				$('.search-result-mh').matchHeight();
        			}
        		});
        		
        		return false;
        	});
        });
        
    </script>

    <?php echo schrimsher_build_header('availability-search', $post->ID); ?>
    
    
    
    <div id="availability-search">
        
        <div class="container-fluid">
            
            <div class="row">
                
                <div class="col-xs-12 col-sm-4 col-md-3 search-filters">
                    
                    <div class="search-filters-form">
                    
                        <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
                            <h3>Refine Search.</h3>
                            
                            <div class="form-group">
                            	<label for="typefilter_control">Property Use (Required):</label>
                        	<?php
                        		if( $terms = get_terms( 'property_type', 'orderby=name' ) ) : // to make it simple I use default categories
                        			echo '<div class="styled-select"><select class="form-control input-lg" name="typefilter" id="typefilter_control">';
                                    echo '<option value="" selected>All Uses</option>';
                        			foreach ( $terms as $term ) :
                        				echo '<option value="' . $term->term_id . '"'.( $_POST['typefilter'] == $term->term_id ? ' selected' : '').'>' . $term->name . '</option>'; // ID of the category as the value of an option
                        			endforeach;
                        			echo '</select></div>';
                        		endif;
                        	?>
                        	</div>
                            
                            <div class="form-group">
                                <label for="areafilter_control">Area to Search:</label>
                            <?php
                        		if( $terms = get_terms( 'property_area', 'orderby=name' ) ) : // to make it simple I use default categories
                        			echo '<div class="styled-select"><select class="form-control input-lg" name="areafilter" id="areafilter_control"><option value="0">All of Huntsville</option>';
                        			foreach ( $terms as $term ) :
                        				echo '<option value="' . $term->term_id . '"'.( $_POST['areafilter'] == $term->term_id ? ' selected' : '').'>' . $term->name . '</option>'; // ID of the category as the value of an option
                        			endforeach;
                        			echo '</select></div>';
                        		endif;
                        	?>
                            </div>
                        	
                        	
                        	
                        	<div class="form-group">
	                        	
	                        	<label for="areafilter_control">Square Footage:</label>
                                        
                            	<div class="styled-select size-select">
                            
                                	<select class="form-control input-lg" name="sizefilter">
	                                	
	                                	<?php 
		                                	$sizes[] = array(
			                                	'value' => '0',
			                                	'label' => 'All Sizes',
		                                	);
		                                	$sizes[] = array(
			                                	'value' => '1-4999',
			                                	'label' => 'Less than 5,000 Sq Ft',
		                                	);
		                                	$sizes[] = array(
			                                	'value' => '5000-10000',
			                                	'label' => '5,000 - 10,000 Sq Ft',
		                                	);
		                                	$sizes[] = array(
			                                	'value' => '10001-999999999999',
			                                	'label' => 'More than 10,000 Sq Ft',
		                                	);
		                                	
		                                	foreach ($sizes as $size) {
			                                	echo '<option value="' . $size['value'] . '"'.( $_POST['sizefilter'] == $size['value'] ? ' selected' : '').'>' . $size['label'] . '</option>';
		                                	}
	                                	?>
                                	
                                	</select>
                                
                                </div>
                            
                            </div>
                            
                            <button class="btn btn-lg btn-primary btn-block">Update Results</button>
                            <input type="hidden" name="action" value="myfilter">
                            
                        </form>
                    
                    </div>
                    
                </div>
                
                <div class="col-xs-12 col-sm-8 col-md-9 search-results">
                    <div id="response">
                    <h2>Search Results</h2>
                    <?php if (!empty($_POST)) { ?>
                    <?php misha_filter_function($_POST); ?>
                    <?php } else { ?>
                    <p class="lead">Use the "Refine Search" box to search for current availabilities at our properties.</p>
                    <?php } ?>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </div>
    
    <?php echo schrimsher_build_inquiry_modal(); ?>
    
    
    
<?php get_footer(); ?>