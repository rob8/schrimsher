<?php 
/* 
Template Name: Availabilities
*/ 
?>

<?php get_header(); ?>

    <script type="text/javascript">
    
        jQuery(function($){
        	$('#filter').submit(function(){
        		var filter = $('#filter');
        		$.ajax({
        			url:filter.attr('action'),
        			data:filter.serialize(), // form data
        			type:filter.attr('method'), // POST
        			beforeSend:function(xhr){
        				//filter.find('button').text('Processing...'); // changing the button label
        			},
        			success:function(data){
        				//filter.find('button').text('Update Results'); // changing the button label back
        				$('#response').html(data); // insert data
        				$('.search-result-mh').matchHeight();
        				$('.search-result-content-mh').matchHeight();
        			}
        		});
        		
        		return false;
        	});
        	autosubmit();
        });
        
    </script>

    <?php echo schrimsher_build_header('headline-wsub', $post->ID); ?>
    
    <div class="page-content">
        
        <div id="availabilities-types">
        
            <div class="container-fluid">
                
                <div class="row text-center">
                    
                    <?php
                    
                        $types = get_post_meta($post->ID, '_page_availabilities_types', true);
                        
                        if (!empty($types)) {
                            echo '<h2>What kind of space are you looking for?</h2>';
                            echo '<form action="'.site_url().'/wp-admin/admin-ajax.php" method="POST" id="filter">';
                            
                            $i = 1;
                            foreach ($types as $type) {
                                echo '<div class="col-xs-6 col-sm-3 text-center type-option">';
                                echo '<input autocomplete="off" class="autosubmit" type="radio" id="type_'.$type['_page_availabilities_type'].'" name="property" value="'.$type['_page_availabilities_type'].'"'.($i == 1 ? ' checked' : '').'>';
                                echo '<label for="type_'.$type['_page_availabilities_type'].'">';
                                
                                echo wp_get_attachment_image( $type['icon_id'], array('200', '80'), "", array( "class" => "img-responsive" ) );
                                echo '<h3>'.$type['title'].'</h3>';
                                echo '</label>';
                                echo '</div>';
                                echo '<input type="hidden" name="action" value="landingfilter">';
                                $i++;
                            }
                            
                            echo '</form>';
                        }
                        
                        
                        
                    ?>
                    
                    <form>
                        
                        
                        
                    </form>
                    
                </div>
                
            </div>
            
        </div>
        
        <div id="availabilities-landing-results">
        
            <div class="container-fluid">
                
                <div class="row">
                        
                        <div id="response">
                           
                            <?php
                                if (!empty($types)) {
                                    $mydata['property'] = $types[0]['_page_availabilities_type'];
                                    echo schrimsher_landingfilter_function($mydata);
                                }
                            ?>
                        </div>
                
                </div>
                
            </div>
        
        </div>
        
        <?php echo schrimsher_space_search('availabilities-find-a-space', 'Looking for something specific? <strong>Narrow your search:</strong>'); ?>
        
    </div>
    
    <?php echo schrimsher_build_inquiry_modal(); ?>
    
<?php get_footer(); ?>