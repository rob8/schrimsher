<?php get_header(); ?>

    <?php echo schrimsher_build_header('newsroom'); ?>

    <div class="container-fluid newsroom-listing">
        
        <div class="row">
            
            <div class="col-xs-12 col-sm-8 newsroom-listing-content">
                
                <div class="row">
            
                <?php
                $i = 1;
                if ( have_posts() ) : while ( have_posts() ) : the_post();
                    
                    echo schrimsher_build_newsroom_cell($post->ID, $i);

            	    $i++;
            
                endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
                
                </div> <!-- row -->
                
            </div> <!-- newsroom-listing-content -->
            
            <div class="col-xs-12 col-sm-4 newsroom-sidebar">
                
                <?php get_sidebar(); ?>
                
            </div>
            
        </div>
        
    </div>
    
<?php get_footer(); ?>