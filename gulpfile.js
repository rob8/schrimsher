var gulp = require('gulp'),
autoprefixer = require('gulp-autoprefixer'),
cssnano = require('gulp-cssnano'),
jshint = require('gulp-jshint'),
uglify = require('gulp-uglify'),
imagemin = require('gulp-imagemin'),
rename = require('gulp-rename'),
concat = require('gulp-concat'),
notify = require('gulp-notify'),
cache = require('gulp-cache'),
livereload = require('gulp-livereload'),
del = require('del'),
less = require('gulp-less');



gulp.task('compile-less', function() {  
  gulp.src('style.less')
    .pipe(less())
    .pipe(autoprefixer('since 2013'))
    .pipe(gulp.dest(''))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest(''))
    .pipe(notify({ message: 'Styles task complete' }));
}); 

gulp.task('watch-less', function() {  
  gulp.watch('./**/*.less' , ['compile-less']);
});