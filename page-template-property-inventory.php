<?php 
/* 
Template Name: Property Inventory
*/ 
?>

<?php get_header(); ?>

    <?php echo schrimsher_build_header('headline-wsub', $post->ID); ?>
    
    <script type="text/javascript">
    
        jQuery(function($){
        	$('#filter').submit(function(){
        		var filter = $('#filter');
        		$.ajax({
        			url:filter.attr('action'),
        			data:filter.serialize(), // form data
        			type:filter.attr('method'), // POST
        			beforeSend:function(xhr){
        				//filter.find('button').text('Processing...'); // changing the button label
        			},
        			success:function(data){
        				//filter.find('button').text('Update Results'); // changing the button label back
        				$('#response').html(data); // insert data
        				$('.search-result-mh').matchHeight();
        				$('.search-result-content-mh').matchHeight();
        			}
        		});
        		
        		return false;
        	});
        	autosubmit();
        });
        
    </script>
    
    <div id="property-inventory-filters">
        
        <div class="container-fluid">
            
            <div class="row">
                   
                <div class="col-xs-12">
                    
                    <?php echo '<form class="form-inline" action="'.site_url().'/wp-admin/admin-ajax.php" method="POST" id="filter">'; ?>
                    
                        <div class="form-group">
                            
                            <label>Property Type:</label>
                            
                            <?php
                                
                                $default_type = "";
                                
                                if (isset($_GET["typefilter"])) { 
                                    $preselected = $_GET["typefilter"];
                                } else {
                                    $preselected = $default_type;
                                }
                                
                                
                                
                                
                                echo '<div class="styled-select use-select">';
                                    if( $terms = get_terms( 'property_type', 'orderby=name' ) ) : // to make it simple I use default categories
                        			echo '<select autocomplete="off" class="form-control autosubmit" name="typefilter" id="typefilter_control">';
                                    echo '<option value="">All Types</option>';
                        			foreach ( $terms as $term ) :
                        				echo '<option value="' . $term->term_id . '"'.($preselected == $term->term_id ? ' selected' : '').'>' . $term->name . '</option>'; // ID of the category as the value of an option
                        			endforeach;
                        			echo '</select>';
                                    endif;
                                echo '</div>';

                            ?>
                            
                        </div>
                        
                        <div class="form-group">
                            
                            <label>Location:</label>
                            
                            <?php
                            
                                echo '<div class="styled-select location-select">';
                                    if( $terms = get_terms( 'property_area', 'orderby=name' ) ) : // to make it simple I use default categories
                        			echo '<select autocomplete="off" class="form-control autosubmit" name="areafilter"><option value="0">All of Huntsville</option>';
                        			foreach ( $terms as $term ) :
                        				echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
                        			endforeach;
                        			echo '</select>';
                                    endif;
                                echo '</div>';

                            ?>
                            
                        </div>
                        
                        <div class="form-group">
                            
                             <label>Size:</label>
                             
                             <div class="styled-select size-select">
                                 
                                 <select autocomplete="off" class="form-control autosubmit" name="sizefilter">
                                     <option value="0">All Sizes</option>
                                     <option value="1">Less than 5,000 SF</option>
                                     <option value="2">5,000 to 10,000 SF</option>
                                     <option value="3">More than 10,000 SF</option>
                                 </select>
                                 
                             </div>
                            
                        </div>
                        
                        <?php echo '<input type="hidden" name="action" value="inventoryfilter">'; ?>
                    
                    </form>
                    
                </div>
                    
            </div>
            
        </div>
        
    </div>
    
    <div id="property-inventory-results">
        
        <div class="container-fluid">
            
            <div class="row">
                    
                    <div id="response">
                       
                        <?php
                            if (isset($_GET["typefilter"])) {
                                $mydata['typefilter'] = $_GET["typefilter"];
                                echo schrimsher_inventoryfilter_function($mydata);
                            } else {
                                $mydata['typefilter'] = $default_type;
                                echo schrimsher_inventoryfilter_function($mydata);
                            }
                        ?>
                    </div>
            
            </div>
            
        </div>
    
    </div>

<?php get_footer(); ?>