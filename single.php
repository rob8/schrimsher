<?php get_header(); ?>

    <?php echo schrimsher_build_header('newsroom-single'); ?>

    <div class="container-fluid newsroom-single">
        
        <div class="row">
            
            <div class="col-xs-12 col-sm-7 newsroom-single-content">
                
                <div class="row">
            
                <?php
                $i = 1;
                if ( have_posts() ) : while ( have_posts() ) : the_post();
                    
                    echo '<h1>'.get_the_title().'</h1>';
                    
                    echo '<div class="item-date">'.get_the_time('F j, Y', $id).'</div>';
                    
                    the_content();
            
                endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
                
                </div> <!-- row -->
                
            </div> <!-- newsroom-listing-content -->
            
            <div class="col-xs-12 col-sm-4 col-sm-offset-1 newsroom-sidebar">
                
                <?php get_sidebar(); ?>
                
            </div>
            
        </div>
        
    </div>
    
<?php get_footer(); ?>