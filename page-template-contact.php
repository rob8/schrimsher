<?php 
/* 
Template Name: Contact Us
*/ 
?>

<?php get_header(); ?>

    <?php echo schrimsher_build_header('headline-wsub', $post->ID); ?>
    
    <div class="page-content">
        
        <div class="container-fluid">
            
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                
                	<?php the_content(); ?>
                
                <?php endwhile; else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
                
            </div>
            
        </div>
        
    </div>
    
    <div class="contact-map">
        
        <div class="container-fluid">
            
            <div class="row">
            
                <div class="col-xs-12 col-md-6 contact-map-info text-center">
                    
                    <div class="inner">
                        
                        <div class="inner-content">
                            
                            <?php 
                                //Footer Logo
                                
                                $logo = get_theme_mod('site_logo');
                                
                                if (!empty($logo)) {
                                    $logo = Kirki_Helper::get_image_id( $logo );
                                    $logo = wp_get_attachment_image( $logo, array('700', '83'), "", array( "class" => "img-responsive" ) );
                                }
                                
                                echo $logo;
                            ?>
                            
                            
                            
                            <?php 
                             
                                $contact_info['addr1'] = get_theme_mod('contact_addr1');
                                $contact_info['addr2'] = get_theme_mod('contact_addr2');
                                $contact_info['addr3'] = get_theme_mod('contact_addr3');
                                $contact_info['phone'] = get_theme_mod('contact_phone');
                                $contact_info['email'] = get_theme_mod('contact_email');
                                $contact_info['fax'] = get_theme_mod('contact_fax');
                                $contact_info['fax_display'] = get_theme_mod('contact_fax_display');
                                
                                $output .= '<p>
                                    '.( !empty($contact_info['addr1']) ? '<span>'.$contact_info['addr1'].'</span>' : '' ).'
                                    '.( !empty($contact_info['addr2']) ? '<span>'.$contact_info['addr2'].'</span>' : '' ).'
                                    '.( !empty($contact_info['addr3']) ? '<span>'.$contact_info['addr3'].'</span>' : '' ).'<br>
                                    '.( !empty($contact_info['phone']) ? '<span>p: '.$contact_info['phone'].'</span>' : '' ).'
                                    '.( !empty($contact_info['email']) ? '<span>e: <a href="mailto:'.$contact_info['email'].'">'.$contact_info['email'].'</a></span>' : '' ).'
                                    '.( !empty($contact_info['fax']) ? '<span>f: '.$contact_info['fax'].'</span>' : '' ).'
                                </p>';
                                
                                echo $output;
                            

                            ?>
                            
                            
                        
                        </div>
                        
                    </div> 
                    
                </div>
                
                <div class="col-xs-12 col-md-6 contact-map-embed">
                    
                    <?php
                        $location_data = get_post_meta($post->ID, '_page_contact_location', true);    
                    ?>
                    
                    <div id='map'></div>
                    <script>
                    function initMap() {
                    <!-- / Styles a map in night mode. -->
                    var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: <?php echo $location_data['lat']; ?>, lng: <?php echo $location_data['lng']; ?>},
                    zoom: 16,
                    scrollwheel: false,
                    mapTypeControl: false,
                    streetViewControl: true,
                    rotateControl: false,
                    zoomControl: false,
                    styles: [
                      {
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#213592"
                          }
                        ]
                      },
                      {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                          {
                            "color": "#ffffff"
                          }
                        ]
                      },
                      {
                        "featureType": "administrative.land_parcel",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#adbdff"
                          }
                        ]
                      },
                      {
                        "featureType": "landscape.man_made",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#e5eaff"
                          }
                        ]
                      },
                      {
                        "featureType": "landscape.natural",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#d3dcff"
                          }
                        ]
                      },
                      {
                        "featureType": "landscape.natural.landcover",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#cad5ff"
                          }
                        ]
                      },
                      {
                        "featureType": "landscape.natural.terrain",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#cad5ff"
                          }
                        ]
                      },
                      {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#c8d2ff"
                          }
                        ]
                      },
                      {
                        "featureType": "poi.business",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#c0ccff"
                          }
                        ]
                      },
                      {
                        "featureType": "poi.government",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#adbdff"
                          }
                        ]
                      },
                      {
                        "featureType": "poi.park",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#adbdff"
                          }
                        ]
                      },
                      {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#566ac4"
                          }
                        ]
                      },
                      {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                          {
                            "color": "#2a42af"
                          }
                        ]
                      },
                      {
                        "featureType": "water",
                        "elementType": "geometry.fill",
                        "stylers": [
                          {
                            "color": "#8a95c9"
                          }
                        ]
                      }
                    ]                            });
                    var image = {
                    url: '<?php bloginfo('template_directory') ?>/images/map-markers/map-marker-default.png',
                    <!-- / This marker is 20 pixels wide by 32 pixels high. -->
                    size: new google.maps.Size(61, 88),
                    <!-- / The origin for this image is (0, 0). -->
                    origin: new google.maps.Point(0, 0),
                    <!-- / The anchor for this image is the base of the flagpole at (0, 32). -->
                    anchor: new google.maps.Point(31, 84)
                    };
                    var marker = new google.maps.Marker({
                    position: {lat: <?php echo $location_data['lat']; ?>, lng: <?php echo $location_data['lng']; ?>},
                    map: map,
                    icon: image,
                    title: 'Schrimsher Properties'
                    });
                    }
                    </script>
                    <script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCsK75xBtXtfJWyPWvElzNQsyTFE5bEmt0&amp;callback=initMap'></script>
                    
                </div>
            
            </div>
        
        </div>
        
    </div>

<?php get_footer(); ?>