<?php 
/* 
Template Name: Robust Properties
*/ 
?>

<?php get_header(); ?>

    <?php echo schrimsher_build_header('intro', $post->ID); ?>
    
    <div class="page-content">
        
        <div class="container-fluid">
            
            <?php 
                
                $output = '';
                
                $properties = get_post_meta($post->ID, '_page_robust_items', true);
                
                $i = 1;
                
                foreach ($properties as $property) {
                    
                    if ($i % 2 == 0) {
                        $class = 'property-even';
                        $meta_class = 'col-sm-push-6';
                        $image_class = 'col-sm-pull-6';
                    } else {
                        $class = 'property-odd';
                        $meta_class = '';
                        $image_class = '';
                    }
                    
                    $content = get_post_meta($property['property'], '_property_attribute_short', true);
        
                    if (!empty($content)) {
                	    
                	    $content = apply_filters('the_content',$content);
                	
                	}
                	
                	$status = $property['status'];
                	
                	$thumb = get_the_post_thumbnail_url( $property['property'], 'header' );
                	
                	$output .= '
                	    <div class="row property '.$class.'">
                
                            <div class="col-xs-12 col-sm-6 property-meta text-center '.$meta_class.'">
                                
                                <div class="flex-container">
                                    
                                    <div class="flex-content">
                                        
                                        <h2>'.get_the_title($property['property']).'</h2>
                                
                                        '.(!empty($content) ? $content : '').'
                                        
                                        <a href="'.get_the_permalink($property['property']).'" class="btn btn-lg">Learn More</a>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                            <div class="col-xs-12 col-sm-6 property-image '.$image_class.'"'.( !empty($thumb) ? ' style="background-image: url('.$thumb.');"' : '' ).'>
                                
                                '.(!empty($status) ? '<div class="item-status">'.$status.'</div>' : '').'
                                
                                
                            </div>
                            
                        </div>
                	';
                    
                    $i++;
                    
                }
                
                echo $output;
                
            ?>
            
        </div>
        
    </div>
    
<?php get_footer(); ?>