<?php get_header(); ?>

    <?php echo schrimsher_build_header('headline-wsub', $post->ID); ?>
    
    <div class="page-content">
        
        <div class="container-fluid">
            
            <div class="row">
            
                <div class="col-xs-12 col-md-9">
                    
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    
                    	<?php the_content(); ?>
                    
                    <?php endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                    
                </div>
            
            </div>
            
        </div>
        
    </div>
    
<?php get_footer(); ?>