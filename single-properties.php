<?php get_header(); ?>

    <?php echo schrimsher_build_header('property', $post->ID); ?>
    
    <div class="page-content">
        
        <div class="property-intro">
        
            <div class="container-fluid">
                
                <div class="row">
                
                    <div class="col-xs-12 col-md-8" data-mh="property-top-group">
                        
                        <h2>Property Overview</h2>
                        
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        
                        	<?php the_content(); ?>
                        
                        <?php endwhile; else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                        <?php endif; ?>
                        
                    </div>
                    
                    <?php
	                	$interested_form = get_theme_mod('interest_form_id');
	                	
	                	if (!empty($interested_form)) {
		                	
		                	?>
		                	
		                		<div class="col-xs-12 col-md-4">
                        
			                        <div class="property-form-container" data-mh="property-top-group">
			                            
			                            <div class="property-form-container-inner">
			                                
			                                <?php echo do_shortcode('[ninja_form id='.$interested_form.']'); ?>
			                                
			                            </div>
			                            
			                        </div>
			                        
			                    </div>
		                	
		                	<?php
		                	
	                	}
	                ?>
                    
           
                
                </div>
                
                <?php 
                    $gallery = get_post_meta($post->ID, '_property_attribute_gallery', true);
                    
                    if (!empty($gallery)) {
                ?>
                
                    <div class="row">
                        
                        <div class="col-xs-12 property-gallery">
                            
                            <?php echo do_shortcode('[envira-gallery id="'.$gallery.'"]'); ?>
                            
                        </div>
                        
                    </div>
                
                <?php
                    }
                ?>
                
            </div>
        
        </div>
        
        <div class="property-availabilities">
        
            <div class="container-fluid">
                
                <div class="row">
                    
                    <div class="col-xs-12">
                        
                        <h2>Availabilities</h2>
                        
                        <?php
                            
                            $args = array(
                                'orderby' => 'menu_order', // we will sort posts by date
                                'order' => 'ASC',
                                'posts_per_page'   => -1,
                                'post_type'        => 'availabilities',
                                'meta_query' => array(
                                    array(
                                        'key' => '_space_attribute_property',
                                        'value' => $post->ID
                                    )
                                )
                            );
                            
                            $availabilities = get_posts($args);
                            
                        ?>
                        
                        <?php if (!empty($availabilities)) { ?>
                        
                            <div class="property-availability-list">
                                
                                <div class="property-availability-head hidden-xs">
                                    
                                    <div class="row">
                                    
                                        <div class="col-xs-12 col-sm-5 item-title">
                                            <div class="flex-container" data-mh="property-item-group">
                                                <div class="flex-content">
                                                    <strong>Description</strong>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-3 item-use">
                                            <div class="flex-container" data-mh="property-item-group">
                                                <div class="flex-content">
                                                    <strong>Use</strong>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-2 item-footage">
                                            <div class="flex-container" data-mh="property-item-group">
                                                <div class="flex-content">
                                                    <strong>Square Footage</strong>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                    
                                </div>
                                
                                <?php
                                    $output = '';
                                    $property_title = get_the_title($post->ID);
                                    foreach ( $availabilities as $post ) : setup_postdata( $post );
                                ?>
                                    <?php
                                        $title = get_the_title($post->ID);
                                        
                                        $id = $post->ID;
                                        
                                        $ft_min = get_post_meta($post->ID, '_space_attribute_sqft_start', true);
                                        $ft_max = get_post_meta($post->ID, '_space_attribute_sqft_end', true);
                                        
                                        if ($ft_min == $ft_max) {
                                            $ft_display = $ft_min.' SF';
                                        } else {
                                            $ft_display = $ft_min.' - '.$ft_max.' SF';
                                        }
                                        
                                        $types = wp_get_post_terms($post->ID, 'property_type', array("fields" => "names"));
                                        if (!empty($types)) {
                                            $types = implode(" / ", $types);
                                        }
                                        
                                        $description = get_post_meta($post->ID, '_space_attribute_description', true);
                                        if (!empty($description)) {
                                    	    $description = apply_filters('the_content',$description);
                                    	}    
                                    ?>
                                    
                                    <?php $output .= '
                                    <div class="property-availability-item">
                                    
                                        <div class="row">
                                        
                                            <div class="col-xs-12 col-sm-5 item-title">
                                                <div class="flex-container" data-mh="property-item-group">
                                                    <div class="flex-content">
                                                        <h3><a class="collapsed" role="button" data-toggle="collapse" href="#collapse'.$id.'" aria-expanded="false" aria-controls="collapse'.$id.'">'.$title.'</a></h3>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-3 item-use">
                                                <div class="flex-container" data-mh="property-item-group">
                                                    <div class="flex-content">
                                                        '.( !empty($types) ? '<i class="fa fa-building" aria-hidden="true"></i> '.$types : '' ).'
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-2 item-footage">
                                                <div class="flex-container" data-mh="property-item-group">
                                                    <div class="flex-content">
                                                        '.( !empty($ft_display) ? '<i class="fa fa-arrows-h" aria-hidden="true"></i> '.$ft_display : '' ).'
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-2 item-inquire text-right">
                                                <div class="flex-container" data-mh="property-item-group">
                                                    <div class="flex-content">
                                                        <a id="inquire-target-'.$post->ID.'" data-toggle="modal" data-target="#inquireModal" class="btn btn-lg btn-primary">Inquire</a>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="item-description collapse col-xs-12 col-sm-8" id="collapse'.$id.'">
                                            
                                                <br>'.( !empty($description) ? $description : '' ).'
                                            
                                            </div>
                                        
                                        </div>
                                        
                                    </div>'; 
                                    
                                    $property_id = get_theme_mod('inquiry_property_field');
                                    $space_id = get_theme_mod('inquiry_space_field');
                                    
                                    $output .= '
                                	<script>
                                	    jQuery( "#inquire-target-'.$post->ID.'" ).click(function() {
                                          var fieldID1 = '.$property_id.';
                                          var newValue1 = \''.$property_title.'\';
                                          jQuery( \'#nf-field-\' + fieldID1 ).val( newValue1 ).trigger( \'change\' );
                                          var fieldID2 = '.$space_id.';
                                          var newValue2 = \''.get_the_title($post->ID).'\';
                                          jQuery( \'#nf-field-\' + fieldID2 ).val( newValue2 ).trigger( \'change\' );
                                        });
                                	</script>    
                                	
                                	';
                                    
                                    ?>
                                    
                                
                                <?php    
                                    endforeach; 
                                    wp_reset_postdata();
                                    echo $output;
                                ?>
                                
                            </div>
                        
                        <?php } else { ?>
                        
                            <p class="lead">There are currently no availabilities at this property.</p>
                        
                        <?php } ?>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        <?php
            $pdfs = get_post_meta($post->ID, '_property_floorplans_items', true);
            if (!empty($pdfs)) {
        ?>
        
            <div class="property-floorplans">
            
                <div class="container-fluid">
                    
                    <div class="row">
                        
                        <div class="col-xs-12">
                            
                            <h2>Floor Plans</h2>
                            
                            <div class="property-floorplans-items">
                                
                                <div class="row">
                                    
                                    <?php
                                    
                                        
                                        
                                        $output = '';
                                        
                                        if (!empty($pdfs)) {
                                            
                                            foreach ($pdfs as $pdf) {
                                                
                                                $output .= '
                                                    <div class="col-xs-12 col-sm-4 floorplan-item">
                                                        <div class="floorplan-item-inner">
                                                            '.(!empty($pdf['pdf']) ? '<a href="'.$pdf['pdf'].'">' : '').wp_get_attachment_image($pdf['image_id'],'highlighted',"", array( "class" => "img-responsive" )).(!empty($pdf['pdf']) ? '</a>' : '').'
                                                            '.(!empty($pdf['pdf']) ? '<a target="_blank" href="'.$pdf['pdf'].'" class="btn btn-lg btn-primary btn-block">'.(!empty($pdf['title']) ? $pdf['title'] : '').'</a>' : '').'
                                                        </div>
                                                    </div>
                                                ';
                                                
                                            }
                                            
                                        }
                                        
                                        echo $output;
                                        
                                    ?>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
        
        <?php } ?>
        
    </div>
    
    <div class="property-map">

        <?php 

            if ( function_exists( 'snapmap_build_single' ) ) {

                $marker = array( 
                    'url' => get_bloginfo('template_directory') . '/images/map-markers/map-marker-default.png', 
                    'size' => '61, 88', 
                    'origin' => '0,0', 
                    'anchor' => '31,84'
                );
                
                echo snapmap_build_single('','100%','400px','16', false, $marker);

            }

        ?>
        
        
        
    </div>
    
    <?php echo schrimsher_build_inquiry_modal(); ?>
    
<?php get_footer(); ?>