<?php get_header(); ?>

    <header id="header-front">
        
        <div class="container-fluid">
            
            <div class="row">
                
                <div class="col-xs-12 col-sm-6 header-front-left">
                    <div class="flex-container">
                        <div class="flex-content text-center">
                            <h2>Schrimsher Company</h2>
                            <p>Commercial Real Estate Development</p>
                            <a class="btn btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 header-front-right">
                    <div class="flex-container">
                        <div class="flex-content text-center">
                            <h2>Schrimsher Properties</h2>
                            <p>Commercial Real Estate Leasing</p>
                            <a class="btn btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
        
    </header>
    
    <?php echo schrimsher_space_search('front-find-a-space'); ?>
    
    <div id="front-featured" class="featured-properties">
        
        <div class="container-fluid">
            
            <div class="row">
                
                <div class="col-xs-12 text-center">
                
                    <h2>Featured Properties</h2>
                
                </div>
                
            </div>
            
            <div class="row">
                
                <?php
                
                    echo schrimsher_featured_property_cell();
                    echo schrimsher_featured_property_cell();
                    echo schrimsher_featured_property_cell();
                    
                ?>
                
            </div>
            
        </div>
        
    </div>
    
    <div id="front-latest-news">
        
        <div class="container-fluid">
            
            <div class="row">
                
                <div class="col-xs-12 text-center">
                    
                    <h2>News &amp; Updates</h2>
                    
                </div>
                
            </div>
            
            <div class="row latest-news-items">
                
                <?php
                    
                    echo schrimsher_latest_news_cell();
                    echo schrimsher_latest_news_cell();
                    echo schrimsher_latest_news_cell();
                        
                ?>
                
            </div>
            
        </div>
        
    </div>

<?php get_footer(); ?>