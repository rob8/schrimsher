<?php 
/* 
Template Name: Properties
*/ 
?>

<?php get_header(); ?>

    <?php echo schrimsher_build_header('intro', $post->ID); ?>
    
    <div id="properties-bar">
        
        <div class="container-fluid">
            
            <div class="row">
                
                <?php echo schrimsher_build_properties_bar_items($post->ID); ?>
                
            </div>
            
        </div>
        
    </div>
    
    <div id="properties-featured" class="featured-properties">
        
        <div class="container-fluid">
            
            <div class="row">
                
                <div class="col-xs-12">
                
                    <h2>Featured Properties</h2>
                
                </div>
                
            </div>
            
            <div class="row">
                
                <?php
                
                    echo schrimsher_build_featured_property_cells();
                    
                ?>
                
            </div>
            
        </div>
        
    </div>
    
    
    
<?php get_footer(); ?>