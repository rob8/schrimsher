<?php
    
    require_once('wp-bootstrap-navwalker.php');
    
	include_once( dirname( __FILE__ ) . '/includes/kirki/kirki.php' );
	
	GLOBAL $theme_ia;
	$theme_ia = get_theme_mod('site_ia');
    
    //-----------------------------------------------------
	// Theme and Post Support
	//-----------------------------------------------------
	
	add_theme_support( 'title-tag' );			// Add theme support for the title tag
	add_theme_support( 'post-thumbnails' );		// Add theme support for post thumbnails
	
	
	//-----------------------------------------------------
	// Enable and Specify Menus
	//-----------------------------------------------------
    
    if ( function_exists( 'register_nav_menus' ) ) {
    	register_nav_menus(
    		array(
    		  'primary_nav' => 'Primary Navigation',
    		  'footer_col1' => 'Footer Navigation (Quick Links)',
    		  'footer_col2' => 'Footer Navigation (Featured Properties)'
    		)
    	);
    }
    
    
    //-----------------------------------------------------
	// Use new jQuery
	//-----------------------------------------------------
	
    function modify_jquery() {
    	if (!is_admin()) {
    		wp_deregister_script('jquery');
    		wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', true, '1.8.1');
    		wp_enqueue_script('jquery');
    	}
    }
    add_action('init', 'modify_jquery');
    
    
    //-----------------------------------------------------
	// Load theme styles and scripts
	//-----------------------------------------------------
	
	function schrimsher_scripts() {
		wp_enqueue_style( 'schrimsher', get_stylesheet_uri(), '', '1.0.1' );
		wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css' );
		wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i' );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'matchHeight', get_template_directory_uri().'/js/jquery.matchHeight-min.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'autosubmit', get_template_directory_uri().'/js/autosubmit.js', array('jquery'), '1.0.0', true );
	}
	
	add_action( 'wp_enqueue_scripts', 'schrimsher_scripts' );
	
	
	
	add_image_size( 'listing', 400, 400, true );
	add_image_size( 'highlighted', 768, 400, true );
	add_image_size( 'blog-cell', 768, 350, true );
	add_image_size( 'header', 1440, 650, true );
	
	
	
	// Add Kirki Configuration

	Kirki::add_config( 'schrimsher', array(
    	'capability'    => 'edit_theme_options',
    	'option_type'   => 'theme_mod',
	) );
	
	// Add Kirki Panel
    
    Kirki::add_panel( 'theme_options', array(
        'priority'    => 10,
        'title'       => __( 'Theme Options', 'schrimsher' ),
        'description' => __( 'Custom options for the Schrimsher theme.', 'schrimsher' ),
	) );
	
	// Add Kirki Sections
    
    Kirki::add_section( 'site_identity', array(
        'title'          => __( 'Site Identity' ),
        'description'    => __( 'Set brand-specific options here..' ),
        'panel'          => 'theme_options', // Not typically needed.
        'priority'       => 160,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
    ) );
    
    Kirki::add_section( 'social_media', array(
        'title'          => __( 'Social Media Accounts' ),
        'description'    => __( 'Specify URLs for social media accounts here.' ),
        'panel'          => 'theme_options', // Not typically needed.
        'priority'       => 160,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
    ) );
    
    Kirki::add_section( 'contact_info', array(
        'title'          => __( 'Contact Information' ),
        'description'    => __( 'Specify your contact information here.' ),
        'panel'          => 'theme_options', // Not typically needed.
        'priority'       => 160,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
    ) );
    
    Kirki::add_section( 'search_options', array(
        'title'          => __( 'Search Options' ),
        'description'    => __( 'If these are already working, these options are best left as-is.' ),
        'panel'          => 'theme_options', // Not typically needed.
        'priority'       => 160,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
    ) );
    
    Kirki::add_section( 'inquiry_options', array(
        'title'          => __( 'Inquiry Form Options' ),
        'description'    => __( 'If these are already working, these options are best left as-is.' ),
        'panel'          => 'theme_options', // Not typically needed.
        'priority'       => 160,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
    ) );
    
    Kirki::add_section( 'newsletter_options', array(
        'title'          => __( 'Newsletter Form Options' ),
        'description'    => __( 'If these are already working, these options are best left as-is.' ),
        'panel'          => 'theme_options', // Not typically needed.
        'priority'       => 160,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
	) );

    if ($theme_ia == 'company') {
        Kirki::add_section( 'project_options', array(
            'title'          => __( 'Project Page Options' ),
            'description'    => __( 'Configure options for the project page template below.' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 160,
            'capability'     => 'edit_theme_options',
            'theme_supports' => '', // Rarely needed.
        ) );
    }
	
	// Add Kirki Fields
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'social_facebook',
    	'label'    => __( 'Facebook', 'schrimsher' ),
    	'section'  => 'social_media',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'social_twitter',
    	'label'    => __( 'Twitter', 'schrimsher' ),
    	'section'  => 'social_media',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'social_instagram',
    	'label'    => __( 'Instagram', 'schrimsher' ),
    	'section'  => 'social_media',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'social_linkedin',
    	'label'    => __( 'LinkedIn', 'schrimsher' ),
    	'section'  => 'social_media',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'contact_addr1',
    	'label'    => __( 'Address', 'schrimsher' ),
    	'section'  => 'contact_info',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'contact_addr2',
    	'label'    => __( 'Address (Line 2)', 'schrimsher' ),
    	'section'  => 'contact_info',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'contact_addr3',
    	'label'    => __( 'Address (Line 3)', 'schrimsher' ),
    	'section'  => 'contact_info',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'contact_phone',
    	'label'    => __( 'Phone', 'schrimsher' ),
    	'section'  => 'contact_info',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'contact_email',
    	'label'    => __( 'Email', 'schrimsher' ),
    	'section'  => 'contact_info',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'contact_fax',
    	'label'    => __( 'Fax', 'schrimsher' ),
    	'section'  => 'contact_info',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'        => 'toggle',
    	'settings'    => 'contact_fax_display',
    	'label'       => __( 'Show fax in footer.', 'schrimsher' ),
    	'section'     => 'contact_info',
    	'default'     => '0',
    	'priority'    => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'        => 'dropdown-pages',
    	'settings'    => 'search_target',
    	'label'       => __( 'What page should availability searches be sent to?', 'schrimsher' ),
    	'section'     => 'search_options',
    	'priority'    => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'        => 'image',
    	'settings'    => 'site_logo',
    	'label'       => __( 'Logo', 'my_textdomain' ),
    	'description' => __( 'Upload your logo here.', 'my_textdomain' ),
    	'section'     => 'site_identity',
    	'default'     => '',
    	'priority'    => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'        => 'switch',
    	'settings'    => 'robust_footer',
    	'label'       => __( 'Robust Footer', 'my_textdomain' ),
    	'section'     => 'site_identity',
    	'default'     => '1',
    	'priority'    => 10,
    	'choices'     => array(
    		'on'  => esc_attr__( 'On', 'my_textdomain' ),
    		'off' => esc_attr__( 'Off', 'my_textdomain' ),
    	),
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'site_copyright',
    	'label'    => __( 'Site Copyright Line', 'schrimsher' ),
    	'section'  => 'site_identity',
    	'priority' => 10,
	) );
	
	Kirki::add_field( 'schrimsher', array(
		'type'        => 'select',
		'settings'    => 'site_ia',
		'label'       => __( 'Information Architecture Theme', 'my_textdomain' ),
		'section'     => 'site_identity',
		'default'     => 'none',
		'priority'    => 10,
		'multiple'    => 1,
		'choices'     => array(
			'none' => esc_attr__( 'None (All Features)', 'my_textdomain' ),
			'properties' => esc_attr__( 'Properties', 'my_textdomain' ),
			'company' => esc_attr__( 'Company', 'my_textdomain' ),
		),
	) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'inquiry_form_id',
    	'label'    => __( 'Ninja Form ID', 'schrimsher' ),
    	'section'  => 'inquiry_options',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'inquiry_property_field',
    	'label'    => __( 'Property Field ID', 'schrimsher' ),
    	'section'  => 'inquiry_options',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'inquiry_space_field',
    	'label'    => __( 'Space Field ID', 'schrimsher' ),
    	'section'  => 'inquiry_options',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'interest_form_id',
    	'label'    => __( 'Interest Ninja Form ID', 'schrimsher' ),
    	'section'  => 'inquiry_options',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'email_form_id',
    	'label'    => __( 'Email Share Ninja Form ID', 'schrimsher' ),
    	'section'  => 'inquiry_options',
    	'priority' => 10,
    ) );
    
    Kirki::add_field( 'schrimsher', array(
    	'type'     => 'text',
    	'settings' => 'newsletter_form_id',
    	'label'    => __( 'Ninja Form ID', 'schrimsher' ),
    	'section'  => 'newsletter_options',
    	'priority' => 10,
    ) );

    if ($theme_ia == 'company') {
        Kirki::add_field( 'schrimsher', array(
            'type'     => 'text',
            'settings' => 'project_availabilities_headline',
            'label'    => __( 'Availabilities Headline Override', 'schrimsher' ),
            'description'    => __( '(Default: Availabilities)', 'schrimsher' ),
            'section'  => 'project_options',
            'priority' => 10,
        ) );

        Kirki::add_field( 'schrimsher', array(
            'type'     => 'text',
            'settings' => 'project_availabilities_text',
            'label'    => __( 'Availabilities Text Override', 'schrimsher' ),
            'description'    => __( 'If you would like to override the callout that appears when a property link has been specified, enter a new callout below. Wrap text that you would like linked in <br>[link]...[/link] <br><br>(Default: To view availabilities for this project, visit [link]it\'s page[/link] on Schrimsher Properties.)', 'schrimsher' ),
            'section'  => 'project_options',
            'priority' => 10,
        ) );
    }
    
    
	
	
	// Register a custom post type for Properties
	
	add_action('init', 'schrimsher_properties');
	function schrimsher_properties() 
	{
	  GLOBAL $theme_ia;
	  $labels = array(
	    'name' => _x('Properties', 'post type general name'),
	    'singular_name' => _x('Property', 'post type singular name'),
	    'add_new' => _x('Add Property', 'book'),
	    'add_new_item' => __('Add New Property'),
	    'edit_item' => __('Edit Property'),
	    'new_item' => __('New Property'),
	    'view_item' => __('View Property'),
	    'search_items' => __('Search Properties'),
	    'not_found' =>  __('No properties found'),
	    'not_found_in_trash' => __('No properties found in Trash'), 
	    'parent_item_colon' => ''
	  );
	  $args = array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => ( $theme_ia == 'company' ? false : true ), 
	    'query_var' => true,
	    'rewrite' => true,
	    'menu_icon' => 'dashicons-admin-multisite',
	    'capability_type' => 'post',
	    'hierarchical' => true,
	    'menu_position' => 5,
	    'supports' => array('title','thumbnail','editor','page-attributes')
	  ); 
	  register_post_type('properties',$args);
	}
	
	
	
	// Register a custom post type for Availabilities
	
	add_action('init', 'schrimsher_availabilities');
	function schrimsher_availabilities() 
	{
	  GLOBAL $theme_ia;
	  $labels = array(
	    'name' => _x('Availabilities', 'post type general name'),
	    'singular_name' => _x('Availability', 'post type singular name'),
	    'add_new' => _x('Add Availability', 'book'),
	    'add_new_item' => __('Add New Availability'),
	    'edit_item' => __('Edit Availability'),
	    'new_item' => __('New Availability'),
	    'view_item' => __('View Availability'),
	    'search_items' => __('Search Availabilities'),
	    'not_found' =>  __('No availabilities found'),
	    'not_found_in_trash' => __('No availabilities found in Trash'), 
	    'parent_item_colon' => ''
	  );
	  $args = array(
	    'labels' => $labels,
	    'public' => false,
	    'publicly_queryable' => false,
	    'show_ui' => ( $theme_ia == 'company' ? false : true ), 
	    'query_var' => false,
	    'rewrite' => false,
	    'menu_icon' => 'dashicons-store',
	    'capability_type' => 'post',
	    'hierarchical' => true,
	    'menu_position' => 5,
	    'supports' => array('title','thumbnail')
	  ); 
	  register_post_type('availabilities',$args);
	}


	// Register a custom post type for Projects
	
	add_action('init', 'schrimsher_projects');
	function schrimsher_projects() 
	{
	  GLOBAL $theme_ia;
	  $labels = array(
	    'name' => _x('Projects', 'post type general name'),
	    'singular_name' => _x('Project', 'post type singular name'),
	    'add_new' => _x('Add Project', 'book'),
	    'add_new_item' => __('Add New Project'),
	    'edit_item' => __('Edit Project'),
	    'new_item' => __('New Project'),
	    'view_item' => __('View Project'),
	    'search_items' => __('Search Projects'),
	    'not_found' =>  __('No projects found'),
	    'not_found_in_trash' => __('No projects found in Trash'), 
	    'parent_item_colon' => ''
	  );
	  $args = array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => ( $theme_ia == 'properties' ? false : true ), 
	    'query_var' => true,
	    'rewrite' => true,
	    'menu_icon' => 'dashicons-admin-multisite',
	    'capability_type' => 'post',
	    'hierarchical' => true,
	    'menu_position' => 5,
	    'supports' => array('title','thumbnail','editor','page-attributes')
	  ); 
	  register_post_type('projects',$args);
	}
	
	
	function schrimsher_property_types() {
    	// create a new taxonomy
    	register_taxonomy(
    		'property_type',
    		array('properties', 'availabilities'),
    		array(
    			'label' => __( 'Property Types' ),
    			'public' => false,
                'rewrite' => false,
                'hierarchical' => true,
                'show_ui' => true,
                'show_admin_column' => true
    		)
    	);
    }
    add_action( 'init', 'schrimsher_property_types' );
    
    add_action( 'init', function() {
    Kirki::add_field( 'schrimsher', array(
    	'type'        => 'select',
    	'settings'    => 'search_type',
    	'label'       => __( 'Which property type should be selected by default on availability search forms?', 'schrimsher' ),
    	'section'     => 'search_options',
    	'default'     => 'option-1',
    	'priority'    => 10,
    	'multiple'    => 1,
    	'choices'     => Kirki_Helper::get_terms( 'property_type' ),
    ) );
    });
    
    function schrimsher_property_areas() {
    	// create a new taxonomy
    	register_taxonomy(
    		'property_area',
    		array('properties', 'availabilities'),
    		array(
    			'label' => __( 'Property Areas' ),
    			'public' => false,
                'rewrite' => false,
                'hierarchical' => true,
                'show_ui' => true,
                'show_admin_column' => true
    		)
    	);
    }
	add_action( 'init', 'schrimsher_property_areas' );
	
	// Remove page templates based on information architecture

	add_filter( 'theme_page_templates', 'schrimsher_remove_page_template' );
    function schrimsher_remove_page_template( $pages_templates ) {
		GLOBAL $theme_ia;
		if ($theme_ia == 'company') {
			unset( $pages_templates['page-template-availabilities.php'] );
			unset( $pages_templates['page-template-availability-search.php'] );
			unset( $pages_templates['page-template-properties.php'] );
			unset( $pages_templates['page-template-property-inventory.php'] );
		}
    	return $pages_templates;
	}
    
    
    function schrimsher_build_header($format, $id = null) {
        
        $output = '';
        
        switch ($format) {
            case 'newsroom':
                $header['classes'] = 'header-450 header-newsroom header-inverse';
                $header['title'] = '<h1>Newsroom</h1>';
                $header['subtitle'] = '';
                $header['form'] = false;
                break;
            case 'newsroom-single':
                $header['classes'] = 'header-450 header-newsroom header-inverse';
                $header['title'] = '<h2>Newsroom</h2>';
                $header['subtitle'] = '';
                $header['form'] = false;
                break;
            case 'headline-wsub':
                // Check for custom headline
                $headline = get_post_meta($id, '_header_headline', true);
                $subhead = get_post_meta($id, '_header_subhead', true);
                if (!empty($subhead)) {
                    $subhead = apply_filters( 'the_content', $subhead );
                }
                $background = get_the_post_thumbnail_url($id, 'header');
                $header['classes'] = 'header-650 header-internal';
                $header['title'] = '<h1>'.( !empty($headline) ? $headline : get_the_title($id) ).'</h1>';
                $header['subtitle'] = ( !empty($subhead) ? $subhead : false );
                $header['form'] = true;
                $header['image'] = ( !empty($background) ? $background : false );
                break;
            case 'property':
                // Check for custom headline
                $headline = get_post_meta($id, '_header_headline', true);
                //$subhead = get_post_meta($id, '_header_subhead', true);
                //if (!empty($subhead)) {
                //    $subhead = apply_filters( 'the_content', $subhead );
                //}
                $page_link = get_the_permalink($id);
				$page_title = str_replace( ' ', '%20', get_the_title($id));
				
				$twitterURL = 'https://twitter.com/intent/tweet?text='.$page_title.'&amp;url='.$page_link;
				$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$page_link;
				$googleURL = 'https://plus.google.com/share?url='.$page_link;
                
                $subhead = '
                	<div class="property-share">
                		<h4>Share this:</h4>
                		<ul>
                			<li class="facebook">
		                      <a href="'.$facebookURL.'" target="_blank">
		                        <i class="fa fa-facebook" aria-hidden="true"></i>
		                        <span class="hidden">
		                          Facebook
		                        </span>
		                      </a>
		                    </li>
		                    <li class="twitter">
		                      <a href="'.$twitterURL.'" target="_blank">
		                        <i class="fa fa-twitter" aria-hidden="true"></i>
		                        <span class="hidden">
		                          Twitter
		                        </span>
		                      </a>
		                    </li>
		                    <li class="gplus">
		                      <a href="'.$googleURL.'" target="_blank">
		                        <i class="fa fa-google-plus" aria-hidden="true"></i>
		                        <span class="hidden">
		                          Google Plus
		                        </span>
		                      </a>
		                    </li>
		                    <li class="email">
		                      <a data-toggle="modal" data-target="#emailModal">
		                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
		                        <span class="hidden">
		                          Email
		                        </span>
		                      </a>
		                    </li>
		                    <!-- <li class="print">
		                      <a href="#email" target="_blank">
		                        <i class="fa fa-print" aria-hidden="true"></i>
		                        <span class="hidden">
		                          Print
		                        </span>
		                      </a>
		                    </li> -->
                		</ul>
                	</div>
                ';
                $background = get_the_post_thumbnail_url($id, 'header');
                $header['classes'] = 'header-600 header-internal';
                $header['title'] = '<h1>'.( !empty($headline) ? $headline : get_the_title($id) ).'</h1>';
                $header['subtitle'] = ( !empty($subhead) ? $subhead : false );
                $header['form'] = false;
                $header['image'] = ( !empty($background) ? $background : false );
                break;
            case 'intro';
                $intro = get_post_meta($id, '_header_intro', true);
                if (!empty($intro)) {
                    $intro = apply_filters( 'the_content', $intro );
                }
                $background = get_the_post_thumbnail_url($id, 'header');
                $header['classes'] = 'header-650 header-internal';
                $header['intro'] = ( !empty($intro) ? $intro : false );
                $header['form'] = true;
                $header['image'] = ( !empty($background) ? $background : false );
                break;
            case 'availability-search':
                // Check for custom headline
                $headline = get_post_meta($id, '_header_headline', true);
                $subhead = get_post_meta($id, '_header_subhead', true);
                if (!empty($subhead)) {
                    $subhead = apply_filters( 'the_content', $subhead );
                }
                $background = get_the_post_thumbnail_url($id, 'header');
                $header['classes'] = 'header-450 header-internal';
                $header['title'] = '<h1>'.( !empty($headline) ? $headline : get_the_title($id) ).'</h1>';
                $header['subtitle'] = ( !empty($subhead) ? $subhead : false );
                $header['form'] = false;
                $header['image'] = ( !empty($background) ? $background : false );
                break;
            default:
                break;
        }
        
        $output .= '
            <header class="'.$header['classes'].'"'.( $header['image'] ? ' style="background-image: url('.esc_url($header['image']).');"' : '' ).'>
                <div class="header-container">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8">
                                '.( !empty($header) ? '<div class="title-container'.( $header['subtitle'] ? ' title-container-wsub' : '').'">'.$header['title'].( $header['intro'] ? '<span class="title-container-intro">'.$header['intro'].'</span>' : '').'</div><br>' : '' ).'
                                '.( $header['subtitle'] ? '<div class="title-container-subhead">'.$header['subtitle'].'</div>' : '').'
                            </div>
                            '.( $header['form'] ? '<div class="col-xs-12 col-sm-4">'.schrimsher_space_search().'</div>' : '' ).'
                        </div>
                    </div>
                </div>
            </header>
        ';
        
        return $output;
        
    }
    
    
    function the_excerpt_trimmed($charlength, $id) {
    	$excerpt = get_the_excerpt($id);
    	$output = '';
    	$charlength++;
    
    	if ( mb_strlen( $excerpt ) > $charlength ) {
    		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
    		$exwords = explode( ' ', $subex );
    		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    		if ( $excut < 0 ) {
    			$output .= mb_substr( $subex, 0, $excut );
    		} else {
    			$output .= $subex;
    		}
    		$output .= '[...]';
    	} else {
    		$output .= $excerpt;
    	}
    	
    	return $output;
    }
    
    
    function schrimsher_build_property_type_array($fancy = false) {
    	    
    	    $terms = get_terms( array(
                'taxonomy' => 'property_type',
                'hide_empty' => false,
            ) );
    	    
    	    
    	    $output = '';
    	    
    	    foreach ($terms as $term) {
        	    if ($fancy) {
            	    $output[$term->term_id] = esc_attr__( $term->name, 'schrimsher' );
        	    } else {
        	        $output["{$term->term_id}"] = $term->name;
        	    }
    	    }
    	    
    	    return $output;
    	
	}
	
    
    function schrimsher_build_property_array() {
    	    
    	    $args = array(
        	    'post_type' => 'properties',
        	    'posts_per_page' => -1
    	    );
    	    
    	    
    	    $options = get_posts($args);
    	    $output = '';
    	    
    	    foreach ($options as $option) {
        	    $output["{$option->ID}"] = $option->post_title;
    	    }
    	    
    	    return $output;
    	
	}

	function schrimsher_build_project_array() {
		
		$args = array(
			'post_type' => 'projects',
			'posts_per_page' => -1
		);
		
		
		$options = get_posts($args);
		$output = '';
		
		foreach ($options as $option) {
			$output["{$option->ID}"] = $option->post_title;
		}
		
		return $output;
	
}
	
	
	function schrimsher_build_envira_array() {
    	    
    	    $args = array(
        	    'post_type' => 'envira',
        	    'posts_per_page' => -1
    	    );
    	    
    	    
    	    $options = get_posts($args);
    	    $output = '';
    	    
    	    foreach ($options as $option) {
        	    $output["{$option->ID}"] = $option->post_title;
    	    }
    	    
    	    return $output;
    	
	}
	
	function schrimsher_social_links($accounts) {
    	    
    	    $output = '';
    	    
    	    foreach ($accounts as $account) {
        	    switch ($account) {
                case 'linkedin':
                    $service['url'] = get_theme_mod('social_linkedin');
                    $service['label'] = 'LinkedIn';
                    $service['icon'] = 'fa-linkedin';
                    $service['class'] = 'linkedin';
                    break;
                case 'twitter':
                    $service['url'] = get_theme_mod('social_twitter');
                    $service['label'] = 'Twitter';
                    $service['icon'] = 'fa-twitter';
                    $service['class'] = 'twitter';
                    break;
                case 'facebook':
                    $service['url'] = get_theme_mod('social_facebook');
                    $service['label'] = 'Facebook';
                    $service['icon'] = 'fa-facebook';
                    $service['class'] = 'facebook';
                    break;
                case 'instagram':
                    $service['url'] = get_theme_mod('social_instagram');
                    $service['label'] = 'Instagram';
                    $service['icon'] = 'fa-instagram';
                    $service['class'] = 'instagram';
                    break;
                default:
                    break;
            }
                
            if (!empty($service['url'])) {  
                $output .= '
                    <li class="'.$service['class'].'">
                      <a href="'.$service['url'].'" target="_blank">
                        <i class="fa '.$service['icon'].'" aria-hidden="true"></i>
                        <span class="hidden">
                          '.$service['label'].'
                        </span>
                      </a>
                    </li>
                ';
            } else {
                $output .= '
                    <li class="'.$service['class'].'" style="display: none; visibility: hidden;">
                        &nbsp;
                    </li>
                ';
            }
            
    	    }
    	    
    	    return $output;
	}
    
    
    function schrimsher_build_newsroom_cell($id, $i) {
        
        $output = '';
            	    
	    $output .= '<div class="newsroom-listing-cell '.( $i == 1 ? 'col-xs-12 newsroom-listing-cell-large': 'col-xs-12 col-sm-6' ).'">';
	    
	    $output .= '<div class="item-inner">';
	    
	    $output .= '<div class="item-image">';
	    
	    $output .= '<div class="item-date">'.get_the_time('F j, Y', $id).'</div>';
	    
	    $output .= get_the_post_thumbnail( $id, ( $i == 1 ? 'large' : 'blog-cell'), array( 'class' => 'img-responsive' ) );
	    
	    $output .= '</div>';
	    
	    $output .= '<div class="item-meta">'; 
	    
	    $output .= '<h2><a href="'.get_the_permalink($id).'">'.get_the_title($id).'</a></h2>';   
	    
	    //if large
	    if ( $i == 1 ) {
	        $output .= '<div class="item-excerpt">'.the_excerpt_trimmed(175, $id).'</div>';
	    }
	    
	    //categories
	    $cats = get_the_category($id);
	    
	    if (!empty($cats)) {
    	    $output .= '<ul class="item-categories">';
    	    foreach ($cats as $cat) {
        	    $output .= '<li><a href="'.esc_url( get_category_link( $cat->term_id ) ).'">'.$cat->name.'</a></li>';
    	    }
    	    $output .= '</ul>';
	    }
	        
	    $output .= '</div>';
	    
	    $output .= '<a class="btn btn-block btn-lg" href="'.get_the_permalink($id).'">Read More</a>';
	    
	    $output .= '</div>';
	    
	    $output .= '</div>';
	    
	    return $output;
    }
    
    
    function schrimsher_build_featured_property_cells() {
        
        $args = array(
        	'posts_per_page'   => 3,
        	'meta_key'         => '_property_visibility_featured',
        	'meta_value'       => 'on',
        	'post_type'        => 'properties'
        );
        
        $output = '';
        
        $properties = get_posts($args);
        
        foreach ($properties as $property) {
            $output .= schrimsher_featured_property_cell($property->ID);
        }
        
        return $output;
    }
    
    
    function schrimsher_featured_property_cell($id) {
        
        $content = get_post_meta($id, '_property_attribute_short', true);
        
        if (!empty($content)) {
    	    
    	    $content = apply_filters('the_content',$content);
    	
    	}
    	
    	$thumb = get_the_post_thumbnail( $id, 'highlighted', array( 'class' => 'img-responsive' ) );
        
        $output = '';
        
        $output .= '
            <div class="col-xs-12 col-sm-4 featured-property">
                    
                <div class="featured-property-inner">
                    
                    <div class="featured-property-content">
                        
                        <div class="featured-property-content-img">
                            
                            <!-- <div class="item-status">Space Available</div> -->
                            
                            '.( !empty($thumb) ? '<a href="'.get_the_permalink($id).'">'.$thumb.'</a>' : '' ).'
                            
                        </div>
                        
                        <div class="featured-property-meta" data-mh="featured-property-group">
                            
                            <h3>'.get_the_title($id).'</h3>
                            
                            '.( !empty($content) ? $content : '' ).'
                            
                        </div>
                        
                    </div>
                    
                    <a href="'.get_the_permalink($id).'" class="btn btn-primary btn-lg btn-block">Learn More</a>
                    
                </div>
                
            </div>
        ';
        
        return $output;
        
    }
    
    
    function schrimsher_latest_news_cell() {
        
        $output = '';
        
        $output .= '
                <div class="col-xs-12 col-sm-4 latest-news-item">
                    
                    <h3><a href="#">The Epic Post Title About Commercial Real Estate</a></h3>
                    
                    <div class="item-date">July 21st, 2017</div>
                    
                    <p>Vivamus rhoncus ullamcorper mattis. Ut euismod eros at blandit imperdiet. Etiam semper mi eu odio semper varius. Nullam posuere mi rhoncus nisl consectetur, in lobortis felis feugiat […]</p>
                    
                    <a href="#" class="item-read-more">Read More</a>
                    
                </div>
        ';
        
        return $output;
        
    }
    
    function schrimsher_build_properties_bar_items($id) {
        $output = '';
        
        $i = 1;
        
        while ($i <= 3) {
            
            $icon_id = get_post_meta($id, '_page_properties_cell'.$i.'_icon_id', true);
            $title = get_post_meta($id, '_page_properties_cell'.$i.'_title', true);
            $content = get_post_meta($id, '_page_properties_cell'.$i.'_content', true);
            $url = get_post_meta($id, '_page_properties_cell'.$i.'_url', true);
            
            $icon = wp_get_attachment_image( $icon_id, array('200', '80'), "", array( "class" => "img-responsive" ) );
            
            if (!empty($content)) {    	    
        	    $content = apply_filters('the_content',$content);
        	}
        	
        	$output .= '
        	    <div class="col-xs-12 col-sm-4 text-center properties-bar-item">
                    
                    <div class="properties-bar-item-content" data-mh="properties-bar-content-group">
                    
                        <div class="flex-container" data-mh="properties-bar-group">
                            
                            <div class="flex-content">
                                
                                '.$icon.'
                                
                            </div>
                            
                        </div>
                        
                        <h3>'.$title.'</h3>
                        
                        '.$content.'
                    
                    </div>
                    
                    <div class="button-container">
                    
                    <a class="btn btn-lg btn-block" href="'.$url.'">Learn More</a>
                    </div>
                    
                </div>
        	';
            
            $i++;
        }
        
        return $output;
    }
    
    
    function schrimsher_space_search($format = 'sidebar', $message = false) {
        
        $output = '';
        
        $search_target = get_theme_mod('search_target');
        
        $default_type = get_theme_mod('search_type');
        
        if (!empty($search_target)) {
            
            // Pull the URL for the search page
            $search_target = get_the_permalink($search_target);
            
            
            
            if ($format == 'sidebar') {
                
                $output .= '
                    <div class="form-find-a-space">
                            
                        <h2>Find a space.</h2>
                        
                        <div class="row">
                        
                            <form action="'.$search_target.'" method="POST">
                                
                                <div class="form-group">
                                    
                                    <div class="col-xs-12">
                                        <div class="styled-select use-select">';
                                            if( $terms = get_terms( 'property_type', 'orderby=name' ) ) : // to make it simple I use default categories
                                			$output .= '<select class="form-control input-lg" name="typefilter" id="typefilter_control">';
                                            $output .= '<option value="" selected>All Uses</option>';
                                			foreach ( $terms as $term ) :
                                				$output .= '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
                                			endforeach;
                                			$output .= '</select>';
                                		endif;
                                        $output .= '</div>
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                    
                                    <div class="col-xs-12">
                                        <div class="styled-select location-select">';
                                            if( $terms = get_terms( 'property_area', 'orderby=name' ) ) : // to make it simple I use default categories
                                			$output .= '<select class="form-control input-lg" name="areafilter"><option value="0">All of Huntsville</option>';
                                			foreach ( $terms as $term ) :
                                				$output .= '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
                                			endforeach;
                                			$output .= '</select>';
                                		endif;
                                        $output .= '</div>
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                
                                	<div class="col-xs-12">
                                	
                                        <div class="styled-select size-select">
                                        
                                        	<select class="form-control input-lg" name="sizefilter">
                                        	
                                        		<option value="0">All Sizes</option>
                                        		<option value="1-4999">Less than 5,000 Sq Ft</option>
                                        		<option value="5000-10000">5,000 - 10,000 Sq Ft</option>
                                        		<option value="10001-999999999999">More than 10,000 Sq Ft</option>
                                        	
                                        	</select>
                                        
                                        </div>
                                        
                                    </div>
                                
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    
                                    <div class="col-xs-12">
                                        
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">Search Availabilities</button>
                                        
                                    </div>
                                    
                                </div>
                                
                            </form>
                        
                        </div>
                        
                    </div> <!-- form-find-a-space -->
                ';
                
            } else {
                
                
                $output .= '
                    
                    <div id="'.$format.'" class="horizontal-find-a-space">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <h2>'.($message ? $message : 'Find a Space.').'</h2>
                                </div>
                            </div>
                            <div class="row">
                                <form action="'.$search_target.'" method="POST">
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="form-group">
                                            <div class="styled-select use-select">';
                                            if( $terms = get_terms( 'property_type', 'orderby=name' ) ) : // to make it simple I use default categories
                                			$output .= '<select class="form-control input-lg" name="typefilter" id="typefilter_control">';
                                			foreach ( $terms as $term ) :
                                				$output .= '<option value="' . $term->term_id . '"'.($default_type == $term->term_id ? ' selected' : '').'>' . $term->name . '</option>'; // ID of the category as the value of an option
                                			endforeach;
                                			$output .= '</select>';
                                		endif;
                                        $output .= '</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="form-group">
                                            <div class="styled-select location-select">';
                                            if( $terms = get_terms( 'property_area', 'orderby=name' ) ) : // to make it simple I use default categories
                                			$output .= '<select class="form-control input-lg" name="areafilter"><option value="0">All of Huntsville</option>';
                                			foreach ( $terms as $term ) :
                                				$output .= '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
                                			endforeach;
                                			$output .= '</select>';
                                		endif;
                                        $output .= '</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        
                                        <div class="form-group">
                                        
                                        	<div class="styled-select size-select">
                                        
	                                        	<select class="form-control input-lg" name="sizefilter">
	                                        	
	                                        		<option value="0">All Sizes</option>
	                                        		<option value="1-4999">Less than 5,000 Sq Ft</option>
	                                        		<option value="5000-10000">5,000 - 10,000 Sq Ft</option>
	                                        		<option value="10001-999999999999">More than 10,000 Sq Ft</option>
	                                        	
	                                        	</select>
	                                        
	                                        </div>
                                        
                                        </div>
                                        
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="row text-center">
                                            <button type="submit" class="btn btn-primary btn-lg">Search Availabilities</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                ';
                
            }
            
        }
        
        
        
        return $output;
        
    }
    
    function schrimsher_build_contact_map_info($id) {
    	
    	$output = '';
    	
    	$address_options = array('addr1', 'addr2', 'addr3');
    	$contact_options = array('phone', 'fax', 'email');
    	
    	if (!empty($address_options)) {
        	
    	}
    	
    	$content = get_post_meta($id, '_page_contact_info', true);
    	
    	if (!empty($content)) {
    	    
    	    $content = apply_filters('the_content',$content);
            $output = $content;
    	
    	}
    	
    	return $content;
    	
	}
	
	function schrimsher_build_inquiry_modal() {
    	$output = '';
    	$inquire_form = get_theme_mod('inquiry_form_id');
    	
    	$output .= '
    	    <div class="modal fade" id="inquireModal" tabindex="-1" role="dialog" aria-labelledby="inquireModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="inquireModalLabel">Property Inquiry</h4>
                  </div>
                  <div class="modal-body">
                    '.( !empty($inquire_form) ? do_shortcode('[ninja_form id=4]') : '').'
                  </div>
                  
                </div>
              </div>
            </div>
    	';
    	
    	return $output;
	}
	
	
	function schrimsher_inventoryfilter_function($data=false){
    	
    	$args = array(
    		'orderby' => 'menu_order', // we will sort posts by date
    		'order' => 'ASC',
    		'post_type' => 'properties',
    		'posts_per_page' => -1
    	);
    	
    	if ($data) {
        	$og_status = true;
        	
    	} else {
        	$data = $_POST;
    	}
    	
    	if ( (empty($data['areafilter'])) && (empty($data['typefilter']))  ) {
            // Don't do anything
        } elseif ( (empty($data['areafilter'])) && (!empty($data['typefilter']))  ) {
        	$args['tax_query'] = array(
    			array(
    				'taxonomy' => 'property_type',
    				'field' => 'id',
    				'terms' => $data['typefilter']
    			)
    		);
        } elseif ( (!empty($data['areafilter'])) && (empty($data['typefilter']))  ) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'property_area',
                    'field' => 'id',
                    'terms' => $data['areafilter']
                )
            );
        } else {
            $args['tax_query'] = array(
    			array(
    				'taxonomy' => 'property_type',
    				'field' => 'id',
    				'terms' => $data['typefilter']
    			),
    			array(
    				'taxonomy' => 'property_area',
    				'field' => 'id',
    				'terms' => $data['areafilter']
    			)
    		);
        }
        
        if (!empty($data['sizefilter'])) {
            switch ($data['sizefilter']) {
                case 1:
                    $sf_range = array(1, 4999);
                    break;
                case 2:
                    $sf_range = array(5000, 10000);
                    break;
                case 3:
                    $sf_range = array(10001, 99999999999);
                    break;
                    
                
            }
            
            /*$args['meta_query'] = array(
                        array(
                                'key' => '_property_attribute_sqft',
                                'value' => $sf_range,
                                'compare' => 'BETWEEN',
                                'type' => 'numeric'
                        ),
            	); */
            	
            
            
                $args['meta_query'] = array(
            		'relation' => 'AND',
            		array(
                        'relation' => 'OR',
                        array(
                                'key' => '_property_attribute_sqft_start',
                                'value' => $sf_range[0],
                                'compare' => '<=',
                                'type' => 'numeric'
                        ),
                        array(
                                'key' => '_property_attribute_sqft_start',
                                'value' => $sf_range[1],
                                'compare' => '<=',
                                'type' => 'numeric'
                        ),
            		),
                    array(
                        'relation' => 'OR',
                        array(
                                'key' => '_property_attribute_sqft_end',
                                'value' => $sf_range[0],
                                'compare' => '>=',
                                'type' => 'numeric'
                        ),
                        array(
                                'key' => '_property_attribute_sqft_end',
                                'value' => $sf_range[1],
                                'compare' => '>=',
                                'type' => 'numeric'
                        ),
            		),
            	);
                
            

        }
		
		$myposts = get_posts( $args );
		if (!empty($myposts)) {
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
                
                    $ft_min = get_post_meta($post->ID, '_property_attribute_sqft_start', true);
                    $ft_max = get_post_meta($post->ID, '_property_attribute_sqft_end', true);
                    
                    if ($ft_min == $ft_max) {
                        $ft_display = $ft_min.' SF';
                    } else {
                        $ft_display = $ft_min.' - '.$ft_max.' SF';
                    }
                    
                    $areas = wp_get_post_terms($post->ID, 'property_area', array("fields" => "names"));
                    if (!empty($areas)) {
                        $areas = implode(" / ", $areas);
                    }
                    
                    $types = wp_get_post_terms($post->ID, 'property_type', array("fields" => "names"));
                    if (!empty($types)) {
                        $types = implode(" / ", $types);
                    }
                    
                    
                    
                    $background = get_the_post_thumbnail_url($post->ID, 'listing');
                
                	echo '<div class="search-result col-xs-12 col-sm-6">';
                	echo '<div class="search-result-inner">';
                	echo '<div class="row">';
                	echo '<div class="col-xs-12 col-sm-4 search-result-image">';
                	echo '<div class="search-result-image-container search-result-mh" data-mh="search-result-group"'.( !empty($background) ? ' style="background-image: url('.$background.');"' : '').'>';
                	echo '&nbsp;';
                	echo '&nbsp;</div>  <!-- .search-result-image-container -->';
                	echo '</div>  <!-- .search-result-image -->';
                	echo '<div class="col-xs-12 col-sm-8 search-result-content search-result-mh" data-mh="search-result-group">';
                	echo '<div class="flex-container search-result-content-mh" data-mh="search-result-content-group"><div class="flex-content">';
                	echo '<h3><a href="'.get_the_permalink($post->ID).'">'.get_the_title($post->ID).'</a></h3>';
                	echo '<div class="search-result-meta">';
                	
                	echo ( !empty($areas) ? '<span class="areas"><i class="fa fa-map-marker" aria-hidden="true"></i>
 '.$areas.'</span>' : '');
                	echo ( !empty($types) ? '<span class="types"><i class="fa fa-building" aria-hidden="true"></i> '.$types.'</span>' : '');
                	echo '<span class="footage"><i class="fa fa-arrows-h" aria-hidden="true"></i> '.$ft_display.'</span>';
                	
                	echo '</div>';
                	
                	echo '</div></div>';
                	
                	echo '</div>';
                	echo '</div> <!-- .row -->';
                	echo '<div class="row">';
                	echo '<div class="search-result-buttons"><div class="col-xs-12"><a class="btn btn-primary btn-lg btn-block btn-view" href="'.get_the_permalink($post->ID).'">View Property</a></div></div>';
                	echo '</div> <!-- .row -->';
                	echo '<div class="clearfix"></div></div>';
                	echo '</div> <!-- .search-result -->';
                
                
                
            endforeach; 
            wp_reset_postdata();
            /*echo '
            /<script type="text/javascript">
    
                jQuery(function($){
                    $(\'.search-result-mh\').matchHeight();
                    $(\'.search-result-content-mh\').matchHeight();
                });
            </script>
            ';*/
        } else {
            echo '<p class="lead">No properties matched your criteria.</p>';
        }
        
        if ($og_status) {
            return;
        } else {
    	    wp_die();
    	}
    }
    
    add_action('wp_ajax_inventoryfilter', 'schrimsher_inventoryfilter_function'); 
    add_action('wp_ajax_nopriv_inventoryfilter', 'schrimsher_inventoryfilter_function');
    
    
    
	
	function schrimsher_landingfilter_function($data=false){
    	
    	$args = array(
    		'orderby' => 'menu_order', // we will sort posts by date
    		'order' => 'ASC',
    		'post_type' => 'availabilities',
    		'numberposts' => -1
    	);
    	
    	if ($data) {
        	$og_status = true;
        	
    	} else {
        	$data = $_POST;
    	}
    	
    	$args['tax_query'] = array(
			array(
				'taxonomy' => 'property_type',
				'field' => 'id',
				'terms' => $data['property']
			)
		);
		
		$myposts = get_posts( $args );
		if (!empty($myposts)) {
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
                $property = get_post_meta($post->ID, '_space_attribute_property', true);
                
                if (!empty($property)) {
                    $ft_min = get_post_meta($post->ID, '_space_attribute_sqft_start', true);
                    $ft_max = get_post_meta($post->ID, '_space_attribute_sqft_end', true);
                    
                    if ($ft_min == $ft_max) {
                        $ft_display = $ft_min.' SF';
                    } else {
                        $ft_display = $ft_min.' - '.$ft_max.' SF';
                    }
                    
                    $areas = wp_get_post_terms($post->ID, 'property_area', array("fields" => "names"));
                    if (!empty($areas)) {
                        $areas = implode(" / ", $areas);
                    }
                    
                    $types = wp_get_post_terms($post->ID, 'property_type', array("fields" => "names"));
                    if (!empty($types)) {
                        $types = implode(" / ", $types);
                    }
                    
                    $description = get_post_meta($post->ID, '_space_attribute_short_description', true);
                    if (!empty($description)) {
                	    $description = apply_filters('the_content',$description);
                	}
                    
                    $background = get_the_post_thumbnail_url($property, 'listing');
                
                	echo '<div class="search-result col-xs-12 col-sm-6">';
                	echo '<div class="search-result-inner">';
                	echo '<div class="row">';
                	echo '<div class="col-xs-12 col-sm-4 search-result-image">';
                	echo '<div class="search-result-image-container search-result-mh" data-mh="search-result-group"'.( !empty($background) ? ' style="background-image: url('.$background.');"' : '').'>';
                	echo '&nbsp;';
                	echo '&nbsp;</div>  <!-- .search-result-image-container -->';
                	echo '</div>  <!-- .search-result-image -->';
                	echo '<div class="col-xs-12 col-sm-8 search-result-content search-result-mh" data-mh="search-result-group">';
                	echo '<div class="flex-container search-result-content-mh" data-mh="search-result-content-group"><div class="flex-content">';
                	echo '<h3><a href="'.get_the_permalink($property).'">'.get_the_title($post->ID).'</a></h3>';
                	echo '<div class="search-result-meta">';
                	echo '<span class="property"><i class="fa fa-flag" aria-hidden="true"></i> <a href="'.get_the_permalink($property).'">'.get_the_title($property).'</a></span>';
                	echo ( !empty($areas) ? '<span class="areas"><i class="fa fa-map-marker" aria-hidden="true"></i>
 '.$areas.'</span>' : '');
                	echo ( !empty($types) ? '<span class="types"><i class="fa fa-building" aria-hidden="true"></i> '.$types.'</span>' : '');
                	echo '<span class="footage"><i class="fa fa-arrows-h" aria-hidden="true"></i> '.$ft_display.'</span>';
                	
                	echo '</div>';
                	
                	echo '</div></div>';
                	
                	echo '</div>';
                	echo '</div> <!-- .row -->';
                	echo '<div class="row">';
                	echo '<div class="search-result-buttons"><div class="col-xs-12 col-sm-6 left"><a class="btn btn-primary btn-block btn-lg" id="inquire-target-'.$post->ID.'" data-toggle="modal" data-target="#inquireModal">Inquire</a></div><div class="col-xs-12 col-sm-6 right"><a class="btn btn-primary btn-lg btn-block btn-view" href="'.get_the_permalink($property).'">View Property</a></div></div>';
                	echo '</div> <!-- .row -->';
                	echo '<div class="clearfix"></div></div>';
                	echo '</div> <!-- .search-result -->';
                	
                	$property_id = get_theme_mod('inquiry_property_field');
                	$space_id = get_theme_mod('inquiry_space_field');
                	
                	echo '
                	<script>
                	    jQuery( "#inquire-target-'.$post->ID.'" ).click(function() {
                          var fieldID1 = '.$property_id.';
                          var newValue1 = \''.get_the_title($property).'\';
                          jQuery( \'#nf-field-\' + fieldID1 ).val( newValue1 ).trigger( \'change\' );
                          var fieldID2 = '.$space_id.';
                          var newValue2 = \''.get_the_title($post->ID).'\';
                          jQuery( \'#nf-field-\' + fieldID2 ).val( newValue2 ).trigger( \'change\' );
                        });
                	</script>    
                	
                	';
                }
                
                
            endforeach; 
            wp_reset_postdata();
        } else {
            echo '<p class="lead">No availabilities matched your criteria.</p>';
        }
        
        if ($og_status) {
            return;
        } else {
    	    wp_die();
    	}
    }
    
    add_action('wp_ajax_landingfilter', 'schrimsher_landingfilter_function'); 
    add_action('wp_ajax_nopriv_landingfilter', 'schrimsher_landingfilter_function');
	
	
	function misha_filter_function($data=false){
    	$args = array(
    		'orderby' => 'menu_order', // we will sort posts by date
    		'order' => 'ASC',
    		'post_type' => 'availabilities',
    		'numberposts' => -1
    	);
    	
    	if ($data) {
        	$og_status = true;
    	} else {
        	$data = $_POST;
    	}
     
    	// for taxonomies / categories
    	if( (!empty($data['typefilter'])) || (!empty($data['areafilter'])) ) {
        	
        	if ( (!empty($data['typefilter'])) && (empty($data['areafilter'])) ) {
        	
        		$args['tax_query'] = array(
        			array(
        				'taxonomy' => 'property_type',
        				'field' => 'id',
        				'terms' => $data['typefilter']
        			)
        		);
        		
        		
    		
    		} elseif ( (empty($data['typefilter'])) && (!empty($data['areafilter'])) ) {
        		
        		$args['tax_query'] = array(
        			array(
        				'taxonomy' => 'property_area',
        				'field' => 'id',
        				'terms' => $data['areafilter']
        			)
        		);
        		
        		
        		
    		} else {
        		
        		$args['tax_query'] = array(
        			array(
        				'taxonomy' => 'property_type',
        				'field' => 'id',
        				'terms' => $data['typefilter']
        			),
        			array(
        				'taxonomy' => 'property_area',
        				'field' => 'id',
        				'terms' => $data['areafilter']
        			)
        		);
        		
        		
        		
    		}
        }
        
        if ( !empty($data['sizefilter']) ) {
            
            $sizes = explode("-", $data['sizefilter']);
            
            $args['meta_query'] = array(
        		'relation' => 'AND',
        		array(
                    'relation' => 'OR',
                    array(
                            'key' => '_space_attribute_sqft_start',
                            'value' => $sizes[0],
                            'compare' => '<=',
                            'type' => 'numeric'
                    ),
                    array(
                            'key' => '_space_attribute_sqft_start',
                            'value' => $sizes[1],
                            'compare' => '<=',
                            'type' => 'numeric'
                    ),
        		),
                array(
                    'relation' => 'OR',
                    array(
                            'key' => '_space_attribute_sqft_end',
                            'value' => $sizes[0],
                            'compare' => '>=',
                            'type' => 'numeric'
                    ),
                    array(
                            'key' => '_space_attribute_sqft_end',
                            'value' => $sizes[1],
                            'compare' => '>=',
                            'type' => 'numeric'
                    ),
        		),
        	);
            
        }
        
        
        /*
        
            _space_attribute_property
            _space_attribute_sqft_start
            _space_attribute_sqft_end
            _space_attribute_image
            _space_attribute_description    
            
        */
     
    	
     
    	$myposts = get_posts( $args );
        	if (!empty($myposts)) {
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
                $property = get_post_meta($post->ID, '_space_attribute_property', true);
                
                if (!empty($property)) {
                    $ft_min = get_post_meta($post->ID, '_space_attribute_sqft_start', true);
                    $ft_max = get_post_meta($post->ID, '_space_attribute_sqft_end', true);
                    
                    if ($ft_min == $ft_max) {
                        $ft_display = $ft_min.' SF';
                    } else {
                        $ft_display = $ft_min.' - '.$ft_max.' SF';
                    }
                    
                    $areas = wp_get_post_terms($post->ID, 'property_area', array("fields" => "names"));
                    if (!empty($areas)) {
                        $areas = implode(" / ", $areas);
                    }
                    
                    $types = wp_get_post_terms($post->ID, 'property_type', array("fields" => "names"));
                    if (!empty($types)) {
                        $types = implode(" / ", $types);
                    }
                    
                    $description = get_post_meta($post->ID, '_space_attribute_short_description', true);
                    if (!empty($description)) {
                	    $description = apply_filters('the_content',$description);
                	}
                    
                    $background = get_the_post_thumbnail_url($property, 'listing');
                
                	echo '<div class="search-result">';
                	echo '<div class="row">';
                	echo '<div class="col-xs-12 col-md-4 search-result-image">';
                	echo '<div class="search-result-image-container search-result-mh" data-mh="search-result-group"'.( !empty($background) ? ' style="background-image: url('.$background.');"' : '').'>';
                	echo '<span class="property"><i class="fa fa-flag" aria-hidden="true"></i> <a href="'.get_the_permalink($property).'">'.get_the_title($property).'</a></span>';
                	echo '&nbsp;</div>  <!-- .search-result-image-container -->';
                	echo '</div>  <!-- .search-result-image -->';
                	echo '<div class="col-xs-12 col-md-8 search-result-content search-result-mh" data-mh="search-result-group"><div class="search-result-content-inner">';
                	echo '<h3><a href="'.get_the_permalink($property).'">'.get_the_title($post->ID).'</a></h3>';
                	echo '<div class="search-result-meta">';
                	
                	echo ( !empty($areas) ? '<span class="areas"><i class="fa fa-map-marker" aria-hidden="true"></i>
 '.$areas.'</span>' : '');
                	echo ( !empty($types) ? '<span class="types"><i class="fa fa-building" aria-hidden="true"></i> '.$types.'</span>' : '');
                	echo '<span class="footage"><i class="fa fa-arrows-h" aria-hidden="true"></i> '.$ft_display.'</span>';
                	
                	echo '</div>';
                	echo ( !empty($description) ? $description : '');
                	echo '<div class="search-result-buttons"><div class="row"><div class="col-xs-12 col-sm-6"><a class="btn btn-lg btn-block btn-inquire" id="inquire-target-'.$post->ID.'" data-toggle="modal" data-target="#inquireModal">Inquire About This Space</a></div><div class="col-xs-12 col-sm-6"><a class="btn btn-lg btn-block btn-view" href="'.get_the_permalink($property).'">View Property Page</a></div></div></div>';
                	echo '</div></div>';
                	echo '</div> <!-- .row -->';
                	echo '</div> <!-- .search-result -->';
                	
                	$property_id = get_theme_mod('inquiry_property_field');
                	$space_id = get_theme_mod('inquiry_space_field');
                	
                	echo '
                	<script>
                	    jQuery( "#inquire-target-'.$post->ID.'" ).click(function() {
                          var fieldID1 = '.$property_id.';
                          var newValue1 = \''.get_the_title($property).'\';
                          jQuery( \'#nf-field-\' + fieldID1 ).val( newValue1 ).trigger( \'change\' );
                          var fieldID2 = '.$space_id.';
                          var newValue2 = \''.get_the_title($post->ID).'\';
                          jQuery( \'#nf-field-\' + fieldID2 ).val( newValue2 ).trigger( \'change\' );
                        });
                	</script>    
                	
                	';
                }
            endforeach; 
            wp_reset_postdata();
        } else {
            echo '<p class="lead">No availabilities matched your criteria.</p>';
        }
             
                if ($og_status) {
                    return;
                } else {
            	    wp_die();
            	}
            }
     
     
    add_action('wp_ajax_myfilter', 'misha_filter_function'); 
    add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');
    
    
    // Callback function to insert 'styleselect' into the $buttons array
    function schrimsher_mce_buttons_2( $buttons ) {
    	array_unshift( $buttons, 'styleselect' );
    	return $buttons;
    }
    // Register our callback to the appropriate filter
    add_filter( 'mce_buttons_2', 'schrimsher_mce_buttons_2' );
	
	
	// Callback function to filter the MCE settings
    function schrimsher_mce_before_init_insert_formats( $init_array ) {  
    	// Define the style_formats array
    	$style_formats = array(  
    		// Each array child is a format with it's own settings
    		array(  
    			'title' => 'Button (Primary)',  
    			'selector' => 'a,button',
    			'classes' => 'btn btn-primary'
    			
    		),
    		array(  
    			'title' => 'Button (Large)',  
    			'selector' => 'a,button',
    			'classes' => 'btn-lg'
    			
    		),
    		array(  
    			'title' => 'Lead Paragraph',  
    			'selector' => 'p',
    			'classes' => 'lead'
    			
    		),
    	);  
    	// Insert the array, JSON ENCODED, into 'style_formats'
    	$init_array['style_formats'] = json_encode( $style_formats );  
    	
    	return $init_array;  
      
    } 
    // Attach callback to 'tiny_mce_before_init' 
    add_filter( 'tiny_mce_before_init', 'schrimsher_mce_before_init_insert_formats' );  
    
    
    function ed_metabox_include_default_page( $display, $meta_box ) {
        	if ( ! isset( $meta_box['show_on']['key'] ) ) {
        		return $display;
        	}
        
        	if ( 'default-page-template' !== $meta_box['show_on']['key'] ) {
        		return $display;
        	}
        
        	$post_id = 0;
        
        	// If we're showing it based on ID, get the current ID
        	if ( isset( $_GET['post'] ) ) {
        		$post_id = $_GET['post'];
        	} elseif ( isset( $_POST['post_ID'] ) ) {
        		$post_id = $_POST['post_ID'];
        	}
        
        	if ( ! $post_id ) {
        		return false;
        	}
        
        	$page_template = get_page_template_slug( $post_id );
        	
        	if ( empty($page_template) ) {
            	$is_it_basic = true;
        	} else {
            	$is_it_basic = false;	
        	}
        
        	// there is a front page set and we're on it!
        	return $is_it_basic;
    }
    add_filter( 'cmb2_show_on', 'ed_metabox_include_default_page', 10, 2 );
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_header_hwsub_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_header_hwsub_metabox() {
    	$prefix = '_header_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_header = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Header Options', 'cmb2' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on'      => array( 'key' => 'page-template', 'value' => array('page-template-contact.php', 'page-template-huntsville.php', 'page-template-availability-search.php', 'page-template-availabilities.php', 'page-template-property-inventory.php' )),
    	) );
    	
    	$cmb_header->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a headline for this page. If left blank, the page title will be used.', 'cmb2' ),
    		'id'   => $prefix . 'headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_header->add_field( array(
    		'name' => esc_html__( 'Subhead Content', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a short intro for this page to show directly below the headline.', 'cmb2' ),
    		'id'   => $prefix . 'subhead',
    		'type' => 'wysiwyg',
    	) );
    	
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_header_defaultpage_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_header_defaultpage_metabox() {
    	$prefix = '_header_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_header = new_cmb2_box( array(
    		'id'            => $prefix . 'basic_metabox',
    		'title'         => esc_html__( 'Header Options', 'cmb2' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on' => array( 'key' => 'default-page-template', 'value' => '' ),
    	) );
    	
    	$cmb_header->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a headline for this page. If left blank, the page title will be used.', 'cmb2' ),
    		'id'   => $prefix . 'headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_header->add_field( array(
    		'name' => esc_html__( 'Subhead Content', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a short intro for this page to show directly below the headline.', 'cmb2' ),
    		'id'   => $prefix . 'subhead',
    		'type' => 'wysiwyg',
    	) );
    	
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_header_property_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_header_property_metabox() {
    	$prefix = '_header_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_header = new_cmb2_box( array(
    		'id'            => $prefix . '_properties_metabox',
    		'title'         => esc_html__( 'Header Options', 'cmb2' ),
    		'object_types'  => array( 'properties','projects' ), // Post type
    	) );
    	
    	$cmb_header->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a headline for this page. If left blank, the page title will be used.', 'cmb2' ),
    		'id'   => $prefix . 'headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_header->add_field( array(
    		'name' => esc_html__( 'Subhead Content', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a short intro for this page to show directly below the headline.', 'cmb2' ),
    		'id'   => $prefix . 'subhead',
    		'type' => 'wysiwyg',
    	) );
    	
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_header_intro_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_header_intro_metabox() {
    	$prefix = '_header_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_header_intro = new_cmb2_box( array(
    		'id'            => $prefix . 'intro_metabox',
    		'title'         => esc_html__( 'Header Options', 'cmb2' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on'      => array( 'key' => 'page-template', 'value' => array('page-template-development.php', 'page-template-properties.php')),
    	) );
    	
    	$cmb_header_intro->add_field( array(
    		'name' => esc_html__( 'Intro Content', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a short intro to display in the header of this page.', 'cmb2' ),
    		'id'   => $prefix . 'intro',
    		'type' => 'wysiwyg',
    	) );
    	
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_page_contact_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_page_contact_metabox() {
    	$prefix = '_page_contact_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_contact = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Contact Page Options', 'cmb2' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-contact.php' ),
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name'       => esc_html__( 'Location', 'pixelwatt' ),
    		'desc'       => esc_html__( 'Select the location of Schrimsher Company.', 'pixelwatt' ),
    		'id'         => $prefix . 'location',
    		'limit_drawing' => true,
    		'type'       => 'snapmap',
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name'    => 'Contact Info',
        	'id'      => $prefix . 'info',
        	'type'    => 'wysiwyg',
    		'options' => array(
        		'media_buttons' => true, // show insert/upload button(s)
        	    'textarea_rows' => 8, // rows="..."
        	    'teeny' => false, // output the minimal editor config used in Press This
        	),
    	) );
    	
    }
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_page_company_landing_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_page_company_landing_metabox() {
    	$prefix = '_page_company_landing_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_company_landing = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Company Landing Template Options', 'cmb2' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-company-landing.php' ),
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'     => esc_html__( 'Featured Cell 1', 'cmb2' ),
    		'desc'     => esc_html__( 'Configure the first of two feature cells.', 'cmb2' ),
    		'id'       => $prefix . 'cell1_extra_info',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a headline for the first feature cell.', 'cmb2' ),
    		'id'   => $prefix . 'cell1_headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'    => esc_html__( 'Image', 'cmb2' ),
    		'desc'    => esc_html__( 'Provide a background image for this cell.', 'cmb2' ),
    		'id'      => $prefix . 'cell1_bg',
    		'type'    => 'file',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a link for the first feature cell.', 'cmb2' ),
    		'id'   => $prefix . 'cell1_url',
    		'type' => 'text_url',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link Target', 'cmb2' ),
    		'desc' => esc_html__( 'Open this link in a new window/tab.', 'cmb2' ),
    		'id'   => $prefix . 'cell1_target',
    		'type' => 'checkbox',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'     => esc_html__( 'Featured Cell 2', 'cmb2' ),
    		'desc'     => esc_html__( 'Configure the first of two feature cells.', 'cmb2' ),
    		'id'       => $prefix . 'cell2_extra_info',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a headline for the first feature cell.', 'cmb2' ),
    		'id'   => $prefix . 'cell2_headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'    => esc_html__( 'Image', 'cmb2' ),
    		'desc'    => esc_html__( 'Provide a background image for this cell.', 'cmb2' ),
    		'id'      => $prefix . 'cell2_bg',
    		'type'    => 'file',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link', 'cmb2' ),
    		'desc' => esc_html__( 'Enter a link for the first feature cell.', 'cmb2' ),
    		'id'   => $prefix . 'cell2_url',
    		'type' => 'text_url',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link Target', 'cmb2' ),
    		'desc' => esc_html__( 'Open this link in a new window/tab.', 'cmb2' ),
    		'id'   => $prefix . 'cell2_target',
    		'type' => 'checkbox',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'     => esc_html__( 'Property Introduction', 'cmb2' ),
    		'desc'     => esc_html__( 'Provide content for the Schrimsher Properties introduction.', 'cmb2' ),
    		'id'       => $prefix . 'property_intro_extra_info',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'    => 'Property Intro',
        	'id'      => $prefix . 'property_intro',
        	'type'    => 'wysiwyg',
    		'options' => array(
        		'media_buttons' => true, // show insert/upload button(s)
        	    'textarea_rows' => 8, // rows="..."
        	    'teeny' => false, // output the minimal editor config used in Press This
        	),
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Schrimsher Properties URL', 'cmb2' ),
    		'desc' => esc_html__( 'Enter the URL of Schrimsher Properties to link to the site.', 'cmb2' ),
    		'id'   => $prefix . 'property_url',
    		'type' => 'text_url',
    	) );
    	
    	
    	// Type 01
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'     => esc_html__( 'Property Type Link 1', 'cmb2' ),
    		'id'       => $prefix . 'type1_extra_info',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'id'   => $prefix . 'type1_headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'    => esc_html__( 'Image', 'cmb2' ),
    		'id'      => $prefix . 'type1_icon',
    		'type'    => 'file',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link', 'cmb2' ),
    		'id'   => $prefix . 'type1_url',
    		'type' => 'text_url',
    	) );
    	
    	// Type 02
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'     => esc_html__( 'Property Type Link 2', 'cmb2' ),
    		'id'       => $prefix . 'type2_extra_info',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'id'   => $prefix . 'type2_headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'    => esc_html__( 'Image', 'cmb2' ),
    		'id'      => $prefix . 'type2_icon',
    		'type'    => 'file',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link', 'cmb2' ),
    		'id'   => $prefix . 'type2_url',
    		'type' => 'text_url',
    	) );
    	
    	// Type 03
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'     => esc_html__( 'Property Type Link 3', 'cmb2' ),
    		'id'       => $prefix . 'type3_extra_info',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'id'   => $prefix . 'type3_headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'    => esc_html__( 'Image', 'cmb2' ),
    		'id'      => $prefix . 'type3_icon',
    		'type'    => 'file',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link', 'cmb2' ),
    		'id'   => $prefix . 'type3_url',
    		'type' => 'text_url',
    	) );
    	
    	// Type 04
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'     => esc_html__( 'Property Type Link 4', 'cmb2' ),
    		'id'       => $prefix . 'type4_extra_info',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Headline', 'cmb2' ),
    		'id'   => $prefix . 'type4_headline',
    		'type' => 'text',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name'    => esc_html__( 'Image', 'cmb2' ),
    		'id'      => $prefix . 'type4_icon',
    		'type'    => 'file',
    	) );
    	
    	$cmb_page_company_landing->add_field( array(
    		'name' => esc_html__( 'Link', 'cmb2' ),
    		'id'   => $prefix . 'type4_url',
    		'type' => 'text_url',
    	) );
    	
    }
    
     add_action( 'cmb2_admin_init', 'schrimsher_register_property_visibility_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_property_visibility_metabox() {
    	$prefix = '_property_visibility_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_contact = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Property Visibility', 'cmb2' ),
    		'object_types'  => array( 'properties', ), // Post type
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name' => esc_html__( 'In Development', 'cmb2' ),
    		'desc' => esc_html__( 'This property is in development.', 'cmb2' ),
    		'id'   => $prefix . 'development',
    		'type' => 'checkbox',
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name' => esc_html__( 'Featured', 'cmb2' ),
    		'desc' => esc_html__( 'This property should be prominently displayed as a featured property.', 'cmb2' ),
    		'id'   => $prefix . 'featured',
    		'type' => 'checkbox',
    	) );
    	
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_property_attribute_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_property_attribute_metabox() {
		GLOBAL $theme_ia;
		$prefix = '_property_attribute_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_contact = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Property Attributes', 'cmb2' ),
    		'object_types'  => array( 'properties','projects' ), // Post type
    	) );
		
		if ($theme_ia != 'company') {
			$cmb_page_contact->add_field( array(
				'name'       => esc_html__( 'Location', 'pixelwatt' ),
				'desc'       => esc_html__( 'Select the location of Schrimsher Company.', 'pixelwatt' ),
				'id'         => $prefix . 'location',
				'limit_drawing' => true,
				'type'       => 'snapmap',
			) );
		
			$cmb_page_contact->add_field( array(
				'name' => __( 'Square Footage (Starting At)', 'theme-domain' ),
				'desc' => __( 'Enter the minimum amount of square footage leasable for this space. (Required)', 'msft-newscenter' ),
				'id'   => $prefix . 'sqft_start',
				'type' => 'text',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'required'    => 'required',
				),
				'sanitization_cb' => 'absint',
					'escape_cb'       => 'absint',
			) );
			
			$cmb_page_contact->add_field( array(
				'name' => __( 'Square Footage (Ending At)', 'theme-domain' ),
				'desc' => __( 'Enter the maximum amount of square footage leasable for this space. If the same as the starting value, simply enter it again here. (Required)', 'msft-newscenter' ),
				'id'   => $prefix . 'sqft_end',
				'type' => 'text',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'required'    => 'required',
				),
				'sanitization_cb' => 'absint',
					'escape_cb'       => 'absint',
			) );
		}

        
        
        $cmb_page_contact->add_field( array(
    		'name'             => esc_html__( 'Envira Gallery', 'cmb2' ),
    		'desc'             => esc_html__( 'Select an Envira gallery to display below this Property\'s content.', 'cmb2' ),
    		'id'               => $prefix . 'gallery',
    		'type'             => 'select',
    		'show_option_none' => true,
    		'options'          => schrimsher_build_envira_array(),
            
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name'    => 'Short Description',
        	'id'      => $prefix . 'short',
        	'type'    => 'wysiwyg',
    		'options' => array(
        		'media_buttons' => true, // show insert/upload button(s)
        	    'textarea_rows' => 8, // rows="..."
        	    'teeny' => false, // output the minimal editor config used in Press This
        	),
		) );
		
		if ($theme_ia == 'company') {
			$cmb_page_contact->add_field( array(
				'name' => __( 'Properties Link', 'cmb2' ),
				'desc' => __( 'If this project has an associated property on the properties website, enter the URL to that property here.', 'cmb2' ),
				'id'   => $prefix . 'link',
				'type' => 'text_url',
			) );
		}
    	
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_property_floor_plans_metabox' );

    function schrimsher_register_property_floor_plans_metabox() {
    	$prefix = '_property_floorplans_';
    
    	/**
    	 * Repeatable Field Groups
    	 */
    	$cmb_group = new_cmb2_box( array(
    		'id'           => $prefix . 'metabox',
    		'title'        => esc_html__( 'Property Floor Plans', 'cmb2' ),
    		'object_types' => array( 'properties','projects' ),
    	) );
    
    	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
    	$group_field_id = $cmb_group->add_field( array(
    		'id'          => $prefix . 'items',
    		'type'        => 'group',
    		'description' => esc_html__( 'Add your floor plan PDFs here.', 'cmb2' ),
    		'options'     => array(
    			'group_title'   => esc_html__( 'Floor Plan {#}', 'cmb2' ), // {#} gets replaced by row number
    			'add_button'    => esc_html__( 'Add Another Floor Plan', 'cmb2' ),
    			'remove_button' => esc_html__( 'Remove Floor Plan', 'cmb2' ),
    			'sortable'      => true, // beta
    			// 'closed'     => true, // true to have the groups closed by default
    		),
    	) );
    	
    	$cmb_group->add_group_field( $group_field_id, array(
    		'name'       => esc_html__( 'Display Title', 'cmb2' ),
    		'id'         => 'title',
    		'type'       => 'text',
    		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    	) );
    	
    	$cmb_group->add_group_field( $group_field_id, array(
    		'name' => esc_html__( 'PDF', 'cmb2' ),
    		'id'   => 'pdf',
    		'type' => 'file',
    	) );
    	
    	$cmb_group->add_group_field( $group_field_id, array(
    		'name' => esc_html__( 'Preview Image', 'cmb2' ),
    		'id'   => 'image',
    		'type' => 'file',
    	) );
	}
	
	add_action( 'cmb2_admin_init', 'schrimsher_register_property_hero_metabox' );
	
	function schrimsher_register_property_hero_metabox() {
		$prefix = '_property_hero_';
	
		/**
			* Repeatable Field Groups
			*/
		$cmb_group = new_cmb2_box( array(
			'id'           => $prefix . 'metabox',
			'title'        => esc_html__( 'Footer Hero Image', 'cmb2' ),
			'object_types' => array( 'projects' ),
		) );
	
		$cmb_group->add_field( array(
    		'name' => esc_html__( 'Image', 'cmb2' ),
    		'desc' => esc_html__( 'If you would like to show a full-width image just above the footer, specify that image here.', 'cmb2' ),
    		'id'   => $prefix . 'image',
    		'type' => 'file',
    	) );
	}
	
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_space_attribute_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_space_attribute_metabox() {
		$prefix = '_space_attribute_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_contact = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Space Attributes', 'cmb2' ),
    		'object_types'  => array( 'availabilities', ), // Post type
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name'             => esc_html__( 'Associated Property', 'cmb2' ),
    		'desc'             => esc_html__( 'Select the property that this availability is in. (Required)', 'cmb2' ),
    		'id'               => $prefix . 'property',
    		'type'             => 'select',
    		'show_option_none' => true,
    		'options'          => schrimsher_build_property_array(),
    		'attributes' => array(
                'required'    => 'required',
        	),
            'column' => array(
        		'position' => 2,
        		'name'     => 'Property',
        	),
    	) );
    	
    	
    	$cmb_page_contact->add_field( array(
        	'name' => __( 'Square Footage (Starting At)', 'theme-domain' ),
        	'desc' => __( 'Enter the minimum amount of square footage leasable for this space. (Required)', 'msft-newscenter' ),
        	'id'   => $prefix . 'sqft_start',
        	'type' => 'text',
        	'attributes' => array(
        		'type' => 'number',
        		'pattern' => '\d*',
                'required'    => 'required',
        	),
        	'sanitization_cb' => 'absint',
                'escape_cb'       => 'absint',
        ) );
        
        $cmb_page_contact->add_field( array(
        	'name' => __( 'Square Footage (Ending At)', 'theme-domain' ),
        	'desc' => __( 'Enter the maximum amount of square footage leasable for this space. If the same as the starting value, simply enter it again here. (Required)', 'msft-newscenter' ),
        	'id'   => $prefix . 'sqft_end',
        	'type' => 'text',
        	'attributes' => array(
        		'type' => 'number',
        		'pattern' => '\d*',
                'required'    => 'required',
        	),
        	'sanitization_cb' => 'absint',
                'escape_cb'       => 'absint',
        ) );
        
        $cmb_page_contact->add_field( array(
    		'name' => esc_html__( 'Image', 'cmb2' ),
    		'desc' => esc_html__( 'By default, the image for the associated property is displayed. If you would like to show a different image, upload it here. (Optional)', 'cmb2' ),
    		'id'   => $prefix . 'image',
    		'type' => 'file',
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name'    => esc_html__( 'Short Description', 'cmb2' ),
    		'desc'    => esc_html__( 'Specify a short description to display with in search results. (Optional)', 'cmb2' ),
    		'id'      => $prefix . 'short_description',
    		'type'    => 'wysiwyg',
    		'options' => array( 'textarea_rows' => 5, ),
    	) );
    	
    	$cmb_page_contact->add_field( array(
    		'name'    => esc_html__( 'Full Description', 'cmb2' ),
    		'desc'    => esc_html__( 'Specify a short description to display with this listing on its parent property\'s page. (Optional)', 'cmb2' ),
    		'id'      => $prefix . 'description',
    		'type'    => 'wysiwyg',
    		'options' => array( 'textarea_rows' => 5, ),
		) );
		
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_page_availabilities_metabox' );

    function schrimsher_register_page_availabilities_metabox() {
    	$prefix = '_page_availabilities_';
    
    	/**
    	 * Repeatable Field Groups
    	 */
    	$cmb_group = new_cmb2_box( array(
    		'id'           => $prefix . 'metabox',
    		'title'        => esc_html__( 'Availability Grid Options', 'cmb2' ),
    		'object_types' => array( 'page', ),
    		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-availabilities.php' ),
    	) );
    
    	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
    	$group_field_id = $cmb_group->add_field( array(
    		'id'          => $prefix . 'types',
    		'type'        => 'group',
    		'description' => esc_html__( 'Configure which property types are displayed on the Availabilities landing page here.', 'cmb2' ),
    		'options'     => array(
    			'group_title'   => esc_html__( 'Property Type {#}', 'cmb2' ), // {#} gets replaced by row number
    			'add_button'    => esc_html__( 'Add Another Property Type', 'cmb2' ),
    			'remove_button' => esc_html__( 'Remove Property Type', 'cmb2' ),
    			'sortable'      => true, // beta
    			// 'closed'     => true, // true to have the groups closed by default
    		),
    	) );
    	
    	
    	$cmb_group->add_group_field( $group_field_id, array(
    		'name'             => esc_html__( 'Property Type', 'cmb2' ),
    		'desc'             => esc_html__( 'Select a property type to display availabilities from when selected.', 'cmb2' ),
    		'id'               => $prefix . 'type',
    		'type'             => 'select',
    		'show_option_none' => true,
    		'options'          => schrimsher_build_property_type_array(),
    	) );
    	
    	$cmb_group->add_group_field( $group_field_id, array(
    		'name'       => esc_html__( 'Display Title', 'cmb2' ),
    		'id'         => 'title',
    		'type'       => 'text',
    		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    	) );
    	
    	$cmb_group->add_group_field( $group_field_id, array(
    		'name' => esc_html__( 'Icon', 'cmb2' ),
    		'id'   => 'icon',
    		'type' => 'file',
    	) );
    }
    
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_page_properties_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_page_properties_metabox() {
    	$prefix = '_page_properties_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_properties = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Property Options', 'cmb2' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-properties.php' ),
    	) );
    	
    	// Cell 1
    	
    	$cmb_page_properties->add_field( array(
    		'name'     => esc_html__( 'Cell 1 Content', 'cmb2' ),
    		'id'       => $prefix . 'cell1_title',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name' => esc_html__( 'Icon', 'cmb2' ),
    		'id'   => $prefix . 'cell1_icon',
    		'type' => 'file',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'       => esc_html__( 'Display Title', 'cmb2' ),
    		'id'         => $prefix . 'cell1_title',
    		'type'       => 'text',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'    => esc_html__( 'Content', 'cmb2' ),
    		'id'      => $prefix . 'cell1_content',
    		'type'    => 'wysiwyg',
    		'options' => array( 'textarea_rows' => 5, ),
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'       => esc_html__( 'Link', 'cmb2' ),
    		'id'         => $prefix . 'cell1_url',
    		'type'       => 'text_url',
    	) );
    	
    	// Cell 2
    	
    	$cmb_page_properties->add_field( array(
    		'name'     => esc_html__( 'Cell 2 Content', 'cmb2' ),
    		'id'       => $prefix . 'cell2_title',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name' => esc_html__( 'Icon', 'cmb2' ),
    		'id'   => $prefix . 'cell2_icon',
    		'type' => 'file',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'       => esc_html__( 'Display Title', 'cmb2' ),
    		'id'         => $prefix . 'cell2_title',
    		'type'       => 'text',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'    => esc_html__( 'Content', 'cmb2' ),
    		'id'      => $prefix . 'cell2_content',
    		'type'    => 'wysiwyg',
    		'options' => array( 'textarea_rows' => 5, ),
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'       => esc_html__( 'Link', 'cmb2' ),
    		'id'         => $prefix . 'cell2_url',
    		'type'       => 'text_url',
    	) );
    	
    	// Cell 3
    	
    	$cmb_page_properties->add_field( array(
    		'name'     => esc_html__( 'Cell 3 Content', 'cmb2' ),
    		'id'       => $prefix . 'cell3_title',
    		'type'     => 'title',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name' => esc_html__( 'Icon', 'cmb2' ),
    		'id'   => $prefix . 'cell3_icon',
    		'type' => 'file',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'       => esc_html__( 'Display Title', 'cmb2' ),
    		'id'         => $prefix . 'cell3_title',
    		'type'       => 'text',
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'    => esc_html__( 'Content', 'cmb2' ),
    		'id'      => $prefix . 'cell3_content',
    		'type'    => 'wysiwyg',
    		'options' => array( 'textarea_rows' => 5, ),
    	) );
    	
    	$cmb_page_properties->add_field( array(
    		'name'       => esc_html__( 'Link', 'cmb2' ),
    		'id'         => $prefix . 'cell3_url',
    		'type'       => 'text_url',
    	) );
    	
    }
    
    add_action( 'cmb2_admin_init', 'schrimsher_register_page_robust_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function schrimsher_register_page_robust_metabox() {
		GLOBAL $theme_ia;
		$prefix = '_page_robust_';
    
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_robust = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Robust List Options', 'cmb2' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-development.php' ),
    	) );
    	
    	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
    	$group_field_id = $cmb_page_robust->add_field( array(
    		'id'          => $prefix . 'items',
    		'type'        => 'group',
    		'description' => esc_html__( 'Configure which properties are displayed on this page.', 'cmb2' ),
    		'options'     => array(
    			'group_title'   => esc_html__( 'Property {#}', 'cmb2' ), // {#} gets replaced by row number
    			'add_button'    => esc_html__( 'Add Another Property', 'cmb2' ),
    			'remove_button' => esc_html__( 'Remove Property', 'cmb2' ),
    			'sortable'      => true, // beta
    			// 'closed'     => true, // true to have the groups closed by default
    		),
    	) );
    	
    	
    	$cmb_page_robust->add_group_field( $group_field_id, array(
    		'name'             => esc_html__( 'Property', 'cmb2' ),
    		'desc'             => esc_html__( 'Select a property to display on this row.', 'cmb2' ),
    		'id'               => 'property',
    		'type'             => 'select',
    		'show_option_none' => true,
    		'options'          => ( $theme_ia == 'company' ? schrimsher_build_project_array() : schrimsher_build_property_array() ),
    	) );
    	
    	$cmb_page_robust->add_group_field( $group_field_id, array(
    		'name'       => esc_html__( 'Status', 'cmb2' ),
    		'id'         => 'status',
    		'type'       => 'text',
    	) );
    	
    	
    	
    }
    