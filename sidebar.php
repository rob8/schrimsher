<?php echo schrimsher_space_search(); ?>
                
                <div class="form-email-signup">
                    
                    <h2>Stay Up-To-Date.</h2>
                    
                    <p>Sign up to receive updates on availabilities and new developments!</p>
                    
                    <div class="row">
                    
                        <form>
                            
                            <div class="form-group">
                                
                                <div class="col-xs-12">
                                    
                                    <input type="email" class="form-control input-lg" id="exampleInputName2" placeholder="Enter Email Address...">
                                    
                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                
                                <div class="col-xs-12">
                                    
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Subscribe!</button>
                                    
                                </div>
                                
                            </div>
                            
                        </form>
                        
                    </div>
                    
                </div>