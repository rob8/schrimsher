<?php 
/* 
Template Name: Company - Landing
*/ 
?>

<?php get_header(); ?>

    <?php
        $background = get_the_post_thumbnail_url($post->ID, 'header');
    ?>

    <header class="header-company"<?php echo ( !empty($background) ? ' style="background-image: url('.$background.');"' : ''); ?>>
        
        
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 header-content">
                        <h1>Schrimsher Company</h1>
                    </div>
                </div>
            </div>
       
        
    </header>
    
    <div class="page-content">
        
        <div class="company-landing-intro">
        
            <div class="container-fluid">
                
                <div class="row">
                
                    <div class="col-xs-12 col-md-9">
                        
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        
                        	<?php the_content(); ?>
                        
                        <?php endwhile; else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                        <?php endif; ?>
                        
                    </div>
                
                </div>
                
            </div>
        
        </div>
        
        <div class="company-landing-feature-cells">
            
            <div class="container-fluid">
                
                <div class="row">
                    
                    <?php
                        $output = '';
                        $i = 1;
                        
                        while($i <= 2) {
                            $headline = get_post_meta($post->ID, '_page_company_landing_cell'.$i.'_headline', true);
                            $bg = get_post_meta($post->ID, '_page_company_landing_cell'.$i.'_bg_id', true);
                            if (!empty($bg)) {
                                $bg = wp_get_attachment_image_src($bg, 'highlighted');
                                $bg = $bg[0];
                            }
                            $url = get_post_meta($post->ID, '_page_company_landing_cell'.$i.'_url', true);
                            $target = get_post_meta($post->ID, '_page_company_landing_cell'.$i.'_target', true);
                            
                            if (!empty($url)) {
                                $output .= '
                                    <a href="'.$url.'" class="col-xs-12 col-sm-6"'.( $target == 'on' ? ' target="_blank"' : '' ).( !empty($bg) ? ' style="background-image: url('.$bg.');"' : '' ).'>
                                    
                                    <span class="featurd-cell-title">'.$headline.'</span>
                                    
                                    </a>
                                ';
                            }
                            
                            $i++;
                        }
                        
                        echo $output;
                        
                    ?>
                    
                </div>
                
            </div>
            
        </div>
        
        <?php
            $intro = get_post_meta($post->ID, '_page_company_landing_property_intro', true);
            if (!empty($intro)) {
                $intro = apply_filters( 'the_content', $intro );
            
        ?>
        
            <div class="company-landing-properties">
                
                <div class="container-fluid">
                    
                    <div class="row">
                        
                        <div class="col-xs-12 col-sm-8 properties-content-container">
                            
                            <div class="properties-content">
                            
                                <?php echo $intro; ?>
                                
                                <?php 
                                    $property_url = get_post_meta($post->ID, '_page_company_landing_property_url', true);
                                    
                                    if (!empty($property_url)) {
                                        echo '<a class="properties-link" href="'.$property_url.'" target="_blank">Visit Schrimsher Properties</a>';
                                    }
                                ?>
                            
                            </div>
                            
                        </div>
                        
                        <div class="col-xs-12 col-sm-4">
                            
                            <div class="properties-types">
                                
                                <div class="row">
                            
                                    <?php
                                        
                                        $output = '';
                                        $i = 1;
                                        
                                        while($i <= 4) {
                                            
                                            $headline = get_post_meta($post->ID, '_page_company_landing_type'.$i.'_headline', true);
                                            $icon = get_post_meta($post->ID, '_page_company_landing_type'.$i.'_icon_id', true);
                                            $url = get_post_meta($post->ID, '_page_company_landing_type'.$i.'_url', true);
                                            
                                            $output .= '
                                            
                                                <a class="col-xs-12 col-sm-6 properties-type text-center" href="'.$url.'" target="_blank">
                                                
                                                    <div class="flex-container" data-mh="properties-type-group">
                                                        <div class="flex-content">
                                                            '.(!empty($icon) ? wp_get_attachment_image( $icon, array('700', '70'), "", array( "class" => "img-responsive" ) ) : '&nbsp;').'
                                                        </div>
                                                    </div>
                                                    
                                                    <h4>'.$headline.'</h4>
                                                
                                                </a>
                                            
                                            ';
                                            
                                            $i++;   
                                        }
                                        
                                        echo $output;
                                        
                                    ?>
                                
                                </div>
                            
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
        
        <?php } ?>
        
    </div>
    
<?php get_footer(); ?>